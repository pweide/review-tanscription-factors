"""
use InterPro to download a list of family/superfamily annotations specifically for the human proteome
- family: share common evolutionary origin, similar function, similar sequence, secondary, tertiary structure
- homologous superfamily: share common evolutionary origin, similar structure, might have very low similarity on
sequence level
"""

# standard library modules
import sys, json, ssl
from urllib import request
from urllib.error import HTTPError
import requests
from time import sleep
from datetime import date
import pandas as pd

OUTPUT = "../output/"
DATE = date.today().strftime("%Y%m%d")


###########################
######## FUNCTIONS ########
###########################

def parse_items(items):
    if type(items) == list:
        return ",".join(items)
    return ""


def parse_member_databases(dbs):
    if type(dbs) == dict:
        return ";".join([f"{db}:{','.join(dbs[db])}" for db in dbs.keys()])
    return ""


def parse_go_terms(gos):
    if type(gos) == list:
        return ",".join([go["identifier"] for go in gos])
    return ""


def parse_locations(locations):
    if type(locations) == list:
        return ",".join(
            [",".join([f"{fragment['start']}..{fragment['end']}"
                       for fragment in location["fragments"]
                       ])
             for location in locations
             ])
    return ""


def parse_group_column(values, selector):
    return ",".join([parse_column(value, selector) for value in values])


def parse_column(value, selector):
    if value is None:
        return ""
    elif "member_databases" in selector:
        return parse_member_databases(value)
    elif "go_terms" in selector:
        return parse_go_terms(value)
    elif "children" in selector:
        return parse_items(value)
    elif "locations" in selector:
        return parse_locations(value)
    return str(value)


def output_list(URL, count=False):
    columns_base = ["accession", "name", "source_database", "type", "integrated", "member_databases", "go_terms"]
    if count:
        columns = columns_base + ["protein_count"]
    else:
        columns = columns_base
    results = pd.DataFrame(columns=columns)

    # disable SSL verification to avoid config issues
    context = ssl._create_unverified_context()

    next = URL
    last_page = False

    attempts = 0
    while next:
        try:
            req = request.Request(next, headers={"Accept": "application/json"})
            res = request.urlopen(req, context=context)
            # If the API times out due a long running query
            if res.status == 408:
                # wait just over a minute
                sleep(61)
                # then continue this loop with the same URL
                continue
            elif res.status == 204:
                # no data so leave loop
                break
            payload = json.loads(res.read().decode())
            next = payload["next"]
            attempts = 0
            if not next:
                last_page = True
        except HTTPError as e:
            if e.code == 408:
                sleep(61)
                continue
            else:
                # If there is a different HTTP error, it will re-try 3 times before failing
                if attempts < 3:
                    attempts += 1
                    sleep(61)
                    continue
                else:
                    sys.stderr.write("LAST URL: " + next)
                    raise e

        for i, item in enumerate(payload["results"]):
            row = {i: parse_column(item["metadata"][i], 'metadata.' + i) for i in columns_base}
            if count:
                if "reviewed" in item["proteins"].keys():
                    row["protein_count"] = item["proteins"]["reviewed"]
                else:
                    row["protein_count"] = 0

            results = results.append(row, ignore_index=True)

        # Don't overload the server, give it time before asking for more
        if last_page == False:
            sleep(1)

    return (results)


def load_interpro_entry(URL, entry_type, count=False):
    BASE_URL = f"{URL}?type={entry_type}&page_size=200"
    return (output_list(BASE_URL, count))


#########################################
######## ACTUAL CODE STARTS HERE ########
#########################################

# retrieve interpro families with number of human proteins annotated to the families in the human proteome.
URL = "https://www.ebi.ac.uk:443/interpro/api/entry/InterPro/proteome/uniprot/UP000005640/protein"
df_family = load_interpro_entry(URL, "family", count=True)
df_superfamily = load_interpro_entry(URL, "homologous_superfamily", count=True)
df_proteome_families_counts = pd.concat([df_family, df_superfamily])
df_proteome_families_counts = df_proteome_families_counts.sort_values(by="protein_count", ascending=False)
df_proteome_families_counts.to_csv(OUTPUT + f"0_InterPro_human_families_{DATE}.csv", index=False)

# download the protein entries for the human proteome including the InterPro domain/family annotation
link = "https://www.uniprot.org/uniprot/?query=*&fil=proteome:UP000005640+AND+reviewed:yes+AND+organism:9606&" \
       "columns=id,entry%20name,reviewed,protein%20names,genes,organism,length,database(InterPro)&format=tab"
f = requests.get(link)
with open(OUTPUT + f"0_UniProt_humanproteome_Interpro_{DATE}.tsv", "w") as file:
    file.write(f.text)
