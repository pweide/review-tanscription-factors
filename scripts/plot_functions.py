import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
from itertools import combinations

fontsizes = [18, 15, 12]
fontsizes = [i + 5 for i in fontsizes]
colors = ['#56b3e9', '#e0d316', '#0072b2', '#e69d00', '#cc79a7']
c = mcolors.ColorConverter().to_rgb
blue = c(colors[0])
orange = c(colors[3])


class Overlaps:
    def __init__(self):
        self.entries = {}
        self.numbers = {}
        self.df_combis = pd.DataFrame(columns=["combination", "n", "intersection"])
        self.sets = {}

    def add_entries(self, entries_dict):
        self.entries.update(entries_dict)

        self.numbers = {i: len(j) for i, j in self.entries.items()}

    def get_combinations(self, n=2, subset_keys=None):
        df_entries = []

        keys = self.entries.keys() if subset_keys is None else subset_keys

        for combi in combinations(keys, n):
            df_entries += \
                [["+".join(combi), n, len(set.intersection(*[set(j) for i, j in self.entries.items() if i in combi]))]]

        self.df_combis = pd.concat(
            [self.df_combis, pd.DataFrame(df_entries, columns=["combination", "n", "intersection"])])

        self.df_combis = self.df_combis.drop_duplicates()

    def define_subsets(self, set_dict):
        self.sets = set_dict


def sort_sets_metric(sets, metric="phosphorylation_count", reverse=True):
    # sort the sets by the share of phosphosites annotated
    sorted_set = {}
    for set_name in sets.keys():
        gene_id = sets[set_name].gene_id_base
        tmp = sets[set_name].counts

        all_proteins = len(set(sets[set_name].counts[gene_id]))
        metric_proteins = len(set(tmp.loc[tmp[metric] > 0, gene_id]))

        sorted_set[set_name] = metric_proteins / all_proteins

    # sort from most common to least common
    sorted_set = sorted(sorted_set.items(), key=lambda x: x[1], reverse=reverse)
    return sorted_set


def calculate_ratios(set_obj, labels):
    set_overlap_obj = Overlaps()
    gene_id = set_obj.gene_id_base
    tmp = set_obj.counts
    # subset to only genes with at least one phosphosite in PSP
    tmp = tmp.loc[tmp["phosphorylation_count"] > 0]
    set_overlap_obj.add_entries({labels[0]: set(set_obj.counts[gene_id]),
                                 labels[1]: set(tmp[gene_id]),
                                 labels[2]: set(tmp.loc[tmp["functionalscore>0.5_count"] > 0, gene_id]),
                                 labels[3]: set(tmp.loc[tmp["regulation_count"] > 0, gene_id]),
                                 labels[4]: set(tmp.loc[tmp["functionalscore>0.5+regulatory_count"] > 0, gene_id])})

    return set_overlap_obj


def plot_phospho_annot_sets(sets, ax, save=False, intersect_tfs="no", offset_curated=False,
                            vertical_layout=False, title="Phosphosite annotation of different sets",
                            show_legend=True):
    protein_sets_overlaps = {}

    labels = ["total", "no known effect", "high funct. score", "known effect", "known effect + high funct. score"]

    colors_bar = {labels[1]: "lightgray", labels[3]: colors[0], labels[2]: colors[3], labels[4]: "#d69f3b"}

    if offset_curated:
        sort_names = [i[0] for i in sort_sets_metric(sets, reverse=False) if i[0] != "curated_TFs"]
        ylabels = ["curated list of TFs", ""] + sort_names
    else:
        sort_names = [i[0] for i in sort_sets_metric(sets, reverse=False)]
        ylabels = ["curated list of TFs" if i == "curated_TFs" else i for i in sort_names]

    if offset_curated:
        set_name = "curated_TFs"
        protein_sets_overlaps[set_name] = calculate_ratios(sets[set_name], labels)

        # get ratios - divide numbers by the total number of proteins
        numbers = protein_sets_overlaps[set_name].numbers
        offset = numbers[labels[3]] / numbers["total"] - (numbers[labels[4]] / numbers["total"])
        bar_labels = {labels[1]: [0, numbers[labels[1]] / numbers["total"]],
                      labels[3]: [0, numbers[labels[3]] / numbers["total"]],
                      labels[2]: [offset, numbers[labels[2]] / numbers["total"]],
                      labels[4]: [offset, numbers[labels[4]] / numbers["total"]]}

        name = "curated list of TFs"

        name = "{} (n={})".format(name, numbers["total"])

        ax.barh(name, height=0.5, width=1, left=0, label="total", color="white")
        ax.barh(name, height=0.5, width=bar_labels[labels[1]][1], left=bar_labels[labels[1]][0], label=labels[1],
                color=colors_bar[labels[1]])
        ax.barh(name, height=0.5, width=bar_labels[labels[3]][1], left=bar_labels[labels[3]][0], label=labels[3],
                alpha=1, color=colors_bar[labels[3]])
        ax.barh(name, height=0.5, width=bar_labels[labels[2]][1], left=bar_labels[labels[2]][0], label=labels[2],
                alpha=1, color=colors_bar[labels[2]])
        ax.barh(name, height=0.5, width=bar_labels[labels[4]][1], left=bar_labels[labels[4]][0], label=labels[4],
                alpha=1, color=colors_bar[labels[4]], hatch='//')
        ax.tick_params(axis='both', which='major', labelsize=fontsizes[2])

        ax.barh(" ", height=0.01, width=1, left=0, color="white")

    for set_name in sort_names:

        protein_sets_overlaps[set_name] = calculate_ratios(sets[set_name], labels)

        # get ratios - divide numbers by the total number of proteins
        numbers = protein_sets_overlaps[set_name].numbers
        offset = numbers[labels[3]] / numbers["total"] - (numbers[labels[4]] / numbers["total"])
        bar_labels = {labels[1]: [0, numbers[labels[1]] / numbers["total"]],
                      labels[3]: [0, numbers[labels[3]] / numbers["total"]],
                      labels[2]: [offset, numbers[labels[2]] / numbers["total"]],
                      labels[4]: [offset, numbers[labels[4]] / numbers["total"]]}

        name = "curated list of TFs" if set_name == "curated_TFs" else set_name

        if intersect_tfs in ["show_nTF", "only_show_nTF"] and set_name != "curated_TFs":
            if "curated_TFs" in sets.keys():
                # get the number of proteins intersecting with curated_TFs list
                n_tfs = len(set(sets[set_name].members["HGNC_symbol"].values).intersection(
                    sets["curated_TFs"].members["HGNC_symbol"].values))
            else:
                # assume that the number of genes in the sets were already subsetted for TFs
                n_tfs = len(set(sets[set_name].members["HGNC_symbol"].values))
            name = "{} (n={}, n_TFs={})".format(name, numbers["total"],
                                                n_tfs) if intersect_tfs == "show_nTF" else "{} (n_TFs={})".format(name,
                                                                                                                  n_tfs)
        else:
            name = "{} (n={})".format(name, numbers["total"])

        ax.barh(name, height=0.5, width=1, left=0, label="total", color="white")
        ax.barh(name, height=0.5, width=bar_labels[labels[1]][1], left=bar_labels[labels[1]][0], label=labels[1],
                color=colors_bar[labels[1]])
        ax.barh(name, height=0.5, width=bar_labels[labels[3]][1], left=bar_labels[labels[3]][0], label=labels[3],
                alpha=1, color=colors_bar[labels[3]])
        ax.barh(name, height=0.5, width=bar_labels[labels[2]][1], left=bar_labels[labels[2]][0], label=labels[2],
                alpha=1, color=colors_bar[labels[2]])
        ax.barh(name, height=0.5, width=bar_labels[labels[4]][1], left=bar_labels[labels[4]][0], label=labels[4],
                alpha=1, color=colors_bar[labels[4]], hatch='//')
        ax.tick_params(axis='both', which='major', labelsize=fontsizes[2])

    ax.set_xlabel("Proteins with phospho annotation / total proteins", fontsize=fontsizes[1])

    labels_leg = [labels[3], labels[4], labels[2], labels[1]]
    handles = [mpatches.Patch(facecolor=colors_bar[label], label=label) if label != labels[4] else mpatches.Patch(
        facecolor=colors_bar[label], label=label, hatch="//")
               for label in labels_leg]
    if show_legend:
        ax.legend(handles=handles, fontsize=fontsizes[2], loc='lower left', bbox_to_anchor=(0.0, 1.01), ncol=2,
                  borderaxespad=0, frameon=False)

    if vertical_layout:
        ax.yaxis.set_label_position("right")
        ax.set_ylabel(title, fontsize=fontsizes[1], rotation=270)
        ax.yaxis.set_label_coords(1.03, 0.5)
    else:
        ax.set_title(title, fontsize=fontsizes[0], y=1.06)

    for ytick, label in zip(ax.get_yticklabels(), ylabels):
        if label == "curated list of TFs":
            ytick.set_fontweight('bold')

    if save:
        plt.tight_layout()
        plt.savefig(save + ".png")
        plt.savefig(save + ".pdf")


def makefigure_publ(df_input, xs_dict, ys_dict, rows=None, cols=None, ignore_zero_x=False, ignore_zero_y=False,
                    show_na_x=False, label_y_axis=False, gene_id_base="HGNC_symbol"):

    if show_na_x:
        df = df_input.fillna(-1)
    else:
        df = df_input

    xs = list(xs_dict.keys())
    ys = list(ys_dict.keys())

    # get each combination
    combos = [(i, j) for i in range(len(ys)) for j in range(len(xs))]

    if rows is None and cols is None:
        rws = len(ys)
        clmns = len(xs)
    elif (rows is None) and (cols is not None):
        rws = int(len(combos) / cols)
        clmns = cols
    elif (rows is not None) and (cols is None):
        clmns = int(len(combos) / rows)
        rws = rows
    else:
        clmns = cols
        rws = rows

    fig, axes = plt.subplots(rws, clmns, figsize=(8 * clmns, 7 * rws))

    ax = [axes] if rws < 2 else axes

    count = 0
    for axis_rows, axsi in enumerate(ax):
        for axis_cols, axs in enumerate(axsi):

            x = xs[combos[count][1]]
            y = ys[combos[count][0]]

            # determine the percentiles for the x and y axes relative to the gene_ID_base
            # remove duplicated entries for [gene_ID_base, x] and [gene_ID_base, y] combinations separately
            # determine the percentiles for each axis such that 10% of available data for that metric
            # lie in one bin in the histogram
            # this way the bins are consistent across different combinations of metrics
            df_sub_x = df.loc[~df.duplicated(subset=[gene_id_base, x]), [gene_id_base, x]]
            df_sub_x = df_sub_x.dropna().set_index(gene_id_base)[x]
            df_sub_y = df.loc[~df.duplicated(subset=[gene_id_base, y]), [gene_id_base, y]]
            df_sub_y = df_sub_y.dropna().set_index(gene_id_base)[y]

            # get percentiles only for non 0 values, if there are 0 values they should get an extra bin (s. below)
            percent = np.linspace(0, 100, 11, endpoint=True)
            x_bins = np.unique(
                [np.nanpercentile(df_sub_x[lambda n: n > 0], k, interpolation="higher") for k in percent])
            y_bins = np.unique(
                [np.nanpercentile(df_sub_y[lambda n: n > 0], k, interpolation="higher") for k in percent])

            # for each axis metric subset the dataframe to non duplicate rows in terms of gene_name and that metric
            # since the merging of the metric values to the overall dataframe was done based on different IDs,
            # for one gene_name ID might exist several rows with duplicated entries of that metric because those
            # rows might differ in e.g HGNC_symbol which was the ID the merging was based on
            # by eliminating the duplicated rows I avoid counting results double
            # !!! number of TFs are reported based on the unique gene_ID_base so if two distinct observations exist
            # for the same gene_ID_base both observations will be a datapoint in the plot,
            # but the TF will only be counted once!!!
            # for 2D histogram to work the arrays of each dimension need to have the same dimension
            df_sub = df.loc[~df.duplicated(subset=[gene_id_base, x, y])]
            df_sub = df_sub[[gene_id_base, x]] if x == y else df_sub[[gene_id_base, x, y]]
            df_sub = df_sub.loc[~df_sub[gene_id_base].isna(), ]

            cap_cb = ""
            if (not ignore_zero_x) & (0 in df_sub_x.values):
                # insert 0 as the first bin
                x_bins = np.insert(x_bins, 0, 0)
                # cap_cb += "x"

            if (not ignore_zero_y) & (0 in df_sub_y.values):
                # insert 0 as the first bin
                y_bins = np.insert(y_bins, 0, 0)
                # cap_cb += "y"
            if show_na_x:
                # insert the non-studied proteins in a metric as the first bin
                x_bins = np.insert(x_bins, 0, -1)
                cap_cb = "-1"

            n_total, matr, x_bins_lb, y_bins_lb = plot2dhist(df_sub[x], df_sub[y],
                                                             x_bins, y_bins,
                                                             axs, fig, cap_cb=cap_cb)
            if show_na_x:
                x_bins_lb[0] = "not studied"
                if (not ignore_zero_x) & (0 in df_sub_x.values):
                    # label the 0 bin differently
                    x_bins_lb[1] = "0"
                if (not ignore_zero_y) & (0 in df_sub_y.values):
                    # label the zero bin differently
                    y_bins_lb[0] = "0"
            else:
                if (not ignore_zero_x) & (0 in df_sub_x.values):
                    # label the 0 bin differently
                    x_bins_lb[0] = "0"
                if (not ignore_zero_y) & (0 in df_sub_y.values):
                    # label the zero bin differently
                    y_bins_lb[0] = "0"

            x_bins_percent = np.sum(matr, axis=0)
            x_bins_percent = ["{:.0f}%".format(i / n_total * 100) for i in x_bins_percent]
            y_bins_percent = np.sum(matr, axis=1)
            y_bins_percent = ["{:.0f}%".format(i / n_total * 100) for i in y_bins_percent]

            axs.set_xticklabels([f"{j} ({x_bins_percent[i]})" for i, j in enumerate(x_bins_lb)], rotation=30,
                                ha='right', fontsize=fontsizes[2])

            if axis_cols == 0:
                axs.set_yticklabels([f"{j} ({y_bins_percent[i]})" for i, j in enumerate(y_bins_lb)],
                                    fontsize=fontsizes[2])
            elif label_y_axis:
                axs.set_yticklabels([f"{j} ({y_bins_percent[i]})" for i, j in enumerate(y_bins_lb)],
                                    fontsize=fontsizes[2])
            elif show_na_x & (not ignore_zero_x):
                axs.set_yticklabels([])
            else:
                axs.set_yticklabels([f"{y_bins_percent[i]}" for i, j in enumerate(y_bins_lb)], fontsize=fontsizes[2])

            if ignore_zero_x:
                genes_xmetric_inbasedf = len(df_sub_x[lambda n: n > 0].index.unique())
            else:
                genes_xmetric_inbasedf = len(df_sub_x[lambda n: n >= 0].index.unique())

            if ignore_zero_y:
                genes_ymetric_inbasedf = len(df_sub_y[lambda n: n > 0].index.unique())
            else:
                genes_ymetric_inbasedf = len(df_sub_y[lambda n: n >= 0].index.unique())

            # show number of intersecting TFs if some data is omitted
            if (not show_na_x) | ignore_zero_x | ignore_zero_y:
                # get the number of intersecting TFs that contribute to the data shown in the histogram
                # -> need to apply the same selection criteria as is used for the histogram
                # 1. remove rows that contain NaNs for any of the two metrics
                # 2. remove 0 where removed remove also those rows
                # => use the unique gene_ID_base in the resulting dataframe as intersecting TFs
                df_sub_nna = df_sub.dropna()
                df_sub_nna = df_sub_nna[df_sub_nna[x] > 0] if ignore_zero_x else df_sub_nna
                df_sub_nna = df_sub_nna[df_sub_nna[y] > 0] if ignore_zero_y else df_sub_nna
                n_intersecting_tfs = len(df_sub_nna[gene_id_base].unique())

                axs.set_title('{:.0f} intersecting TFs'.format(n_intersecting_tfs), fontsize=fontsizes[1])
            x_lbl = "{} (n_TFs = {:.0f})".format((xs_dict[x]), genes_xmetric_inbasedf)

            y_lbl = "{} (n_TFs = {:.0f})".format(ys_dict[y], genes_ymetric_inbasedf)

            axs.set_xlabel(x_lbl, fontsize=fontsizes[1])
            if axis_cols == 0:
                axs.set_ylabel(y_lbl, fontsize=fontsizes[1])
            elif label_y_axis:
                axs.set_ylabel(y_lbl, fontsize=fontsizes[1])

            count += 1

    plt.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95, wspace=0.5, hspace=0.5)


def plot2dhist(x, y, x_bins, y_bins, ax, fig, cap_cb=""):
    # when bins for both dimensions are given x,y can have nans but those will be ignored,
    # if bins would not be provided an error would be thrown
    matr, a, b, = np.histogram2d(x, y, bins=[x_bins, y_bins])
    entries = matr.sum()

    matr = matr.T

    maxx = np.max(matr)
    if cap_cb == "":
        vmax = np.max(matr)
    # elif cap_cb == "x":
    #     vmax = np.max(matr[:, 1:])
    # elif cap_cb == "y":
    #     vmax = np.max(matr[1:, :])
    # elif cap_cb == "xy":
    #     vmax = np.max(matr[1:, 1:])
    elif cap_cb == "-1":
        vmax = np.max(matr[1:, 1:])

    # construct the colormap
    if vmax < maxx:
        bp = (vmax+1)/maxx
        cdict = {"red": [[0, 1, 1], [bp, blue[0], blue[0]], [1, orange[0], orange[0]]],
                 "green": [[0, 1, 1], [bp, blue[1], blue[1]], [1, orange[1], orange[1]]],
                 "blue": [[0, 1, 1], [bp, blue[2], blue[2]], [1, orange[2], orange[2]]]}
    else:
        cdict = {"red": [[0, 1, 1], [1, blue[0], 1]],
                 "green": [[0, 1, 1], [1, blue[1], 1]],
                 "blue": [[0, 1, 1], [1, blue[2], 1]]}

    cmap_break = mcolors.LinearSegmentedColormap('cmap_break', cdict)

    #pos = ax.imshow(matr, cmap="Blues", origin="lower", vmax=vmax)
    pos = ax.imshow(matr, cmap=cmap_break, origin="lower")
    cbar = fig.colorbar(pos, ax=ax)
    cbar.ax.tick_params(labelsize=fontsizes[2])
    cbar.set_label('# of combinations', rotation=270, fontsize=fontsizes[2], labelpad=12)

    # Show all ticks...
    ax.set_xticks(np.arange(len(x_bins) - 1))
    ax.set_yticks(np.arange(len(y_bins) - 1))

    x_bins_lb = ["[{:.0f},{:.0f})".format(x_bins[i], x_bins[i + 1]) for i in range(len(x_bins) - 1)]
    x_bins_lb[-1] = x_bins_lb[-1][:-1] + "]"

    y_bins_lb = ["[{:.0f},{:.0f})".format(y_bins[i], y_bins[i + 1]) for i in range(len(y_bins) - 1)]
    y_bins_lb[-1] = y_bins_lb[-1][:-1] + "]"

    # ... and label them with the respective list entries
    ax.set_xticklabels(x_bins_lb, rotation=30, ha='right', fontsize=fontsizes[2])
    ax.set_yticklabels(y_bins_lb, fontsize=fontsizes[2])
    ax.set_aspect('auto')

    return entries, matr, x_bins_lb, y_bins_lb
