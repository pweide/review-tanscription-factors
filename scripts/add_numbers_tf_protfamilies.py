import argparse
import pandas as pd
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt
from supervenn import supervenn
import pickle

from module.ProteinSet import ProteinSet
import module.merge_similar_sets as mss
from module.gene_id_mapping import GeneIdMapping

parser = argparse.ArgumentParser(description="Included TF resources")
parser.add_argument("--resource",
                    choices=["Lambert", "Dorothea", "Trrust", "all"],
                    type=str,
                    help="Resources used for curating a list of TFs")

args = parser.parse_args()
if args.resource is None:
    resource = "all"
else:
    resource = args.resource

OUTPUT = "../output/"
if resource != "all":
    OUTPUT += resource + "/"

INPUT = "../input/"
# top protein families to consider
top_fam_TF = 15
top = 10

# load the human identifier mapping table
mapping_table = GeneIdMapping("../input/biomart_id_mapping.csv")

fontsizes = [18, 15, 12]
colorblindfr = {"main": ['#56b3e9', '#e0d316', '#0072b2', '#e69d00', '#cc79a7'],
                "additional": ['#EC681E', '#009e74', '#000000']}
colors = colorblindfr["main"]

#################################
# LOAD DATAFRAMES
#################################

# load the InterPro families with the protein counts
InterPro_families = pd.read_csv("../output/" + "0_InterPro_human_families_20210309.csv")
interpro_mapping = {acc: InterPro_families.at[i, "name"]
                    for i, acc in enumerate(InterPro_families["accession"])}
proteome_families_counts = dict(zip(InterPro_families["accession"], InterPro_families["protein_count"]))
# sort from most common to least common
proteome_families_counts = sorted(proteome_families_counts.items(), key=lambda x: x[1], reverse=True)

# load the human proteome
human_proteome = pd.read_csv("../output/" + "0_Uniprot_humanproteome_Interpro_20210309.tsv",
                             delimiter="\t",
                             usecols=["Entry", "Protein names", "Gene names", "Cross-reference (InterPro)"])
human_proteome.columns = ['uniprot_ACC', 'protein_names', 'uniprot_entryname', 'InterPro']
# only consider proteins that have interpro annotation
human_proteome_interpro = human_proteome.loc[~human_proteome["InterPro"].isna()]

# load curated TFs list
TFs_curated = pd.read_csv(OUTPUT + "2_TF_interpro_family_annotation.csv")
TFs_curated["InterPro_fam_list"] = [i if i is np.nan else [j for j in i.split(";")[:-1]]
                                    for i in TFs_curated["InterPro_fam"]]

###
# TF phosphosites
###
phosphositeplus = pd.read_table(INPUT + "PhosphositePlus/Phosphorylation_site_dataset.tsv", sep="\t")
# only keep the human records
phosphositeplus = phosphositeplus.loc[phosphositeplus["ORGANISM"] == "human"]

phosphositeplus_regulatory = pd.read_table(INPUT + "PhosphositePlus/Regulatory_sites.tsv", sep="\t")
# only keep the human records
phosphositeplus_regulatory = phosphositeplus_regulatory.loc[phosphositeplus_regulatory["ORGANISM"] == "human"]

# only keep entries regarding phosphorylation: MOD_RSD residue-p
phosphositeplus_regulatory = phosphositeplus_regulatory.loc[
    phosphositeplus_regulatory["MOD_RSD"].str.match('.*-p$') == True]

funct_score_Ochoa = pd.read_excel(INPUT + "Ochoa2020_functionalScoresPhosphosites.xlsx")

#######################################
# GET COMMON PROTEIN FAMILIES AMONG TFS
#######################################

# collect all families associated to the TFs in a big list -> then count the entries
# since the list contains duplicated entries for InterPro families based on which ID is used
# -> subset to individual rows based on the ID (uniprot ACC) used to add the InterPro info to the curatedTF table
TFs_curated_interpro = TFs_curated[(~TFs_curated.duplicated(subset=["uniprot_ACC"])) &
                                   (~TFs_curated["InterPro_fam_list"].isna())]

families_TF_interpro = [j for i in TFs_curated_interpro["InterPro_fam_list"] for j in i]
families_TF_interpro = Counter(families_TF_interpro)

# define protein sets using the 10 most common families found in the TFs
TFs_families = {}
most_comm_fam = [i[0] for i in families_TF_interpro.most_common(top_fam_TF)]
for fam in most_comm_fam:
    # gather the TFs that are annotated to each common family
    tmp = TFs_curated_interpro.loc[TFs_curated_interpro["InterPro_fam_list"].apply(lambda x: fam in x),
                                   "uniprot_ACC"].tolist()
    TFs_families[interpro_mapping[fam]] = tmp

# investigate the overlap between different families
plt.figure(figsize=(30, 0.7 * len(TFs_families)))
keys = [i for i in TFs_families.keys()]
values = [set(TFs_families[i]) for i in keys]
a = supervenn(values, keys, sets_ordering='minimize gaps', side_plots=False,
              min_width_for_annotation=20, fontsize=fontsizes[1])
a.axes["main"].set_ylabel("Sets", size=fontsizes[1])
a.axes["main"].set_xlabel("Proteins", size=fontsizes[1])
plt.savefig(OUTPUT + f"protein_sets/TFs_families_{top_fam_TF}_before_merge.png")
plt.close()

sets_merging, sets_overlapping, sets_merging_names = mss.find_similiar_sets(TFs_families, outside_intersect=-1)

sets_merging, sets_overlapping, sets_merging_names = mss.find_similiar_sets_name(TFs_families)

with open(OUTPUT + f"protein_sets/TFs_families_{top_fam_TF}_mergedfam.txt", "w") as f:
    for i, j in enumerate(sets_merging):
        f.write(f"{j} were merged into new family: '{sets_merging_names[i]}'\n")

TFs_families_merged = mss.merge_similar_sets(TFs_families, sets_merging, sets_merging_names)

plt.figure(figsize=(30, 0.7 * len(TFs_families_merged)))
keys = [i for i in TFs_families_merged.keys()]
values = [set(TFs_families_merged[i]) for i in keys]
a = supervenn(values, keys, sets_ordering='minimize gaps', side_plots=False, min_width_for_annotation=20,
              fontsize=fontsizes[1])
a.axes["main"].set_ylabel("Sets", size=fontsizes[1])
a.axes["main"].set_xlabel("Proteins", size=fontsizes[1])
plt.savefig(OUTPUT + f"protein_sets/TFs_families_{top_fam_TF}_after_merge.png")
plt.close()

TFs_families_merged_sets_TFs = {}
# get phosphorylation counts etc. for the TFs of different TF families
for (set_name, genes) in TFs_families_merged.items():
    print(">>>" + set_name)
    genes_df = mapping_table.convert_id(genes, id_from="uniprot_ACC", id_to="all", retain_nonmap=True)
    tmp = ProteinSet(genes_df, name=set_name)

    tmp.add_phosphosites(phosphositeplus, gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
    tmp.add_regulationannot(phosphositeplus_regulatory, gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
    tmp.add_functionalscore(funct_score_Ochoa, gene_id_data="uniprot", gene_id_members="uniprot_ACC")
    tmp.get_highlyfunctionalsites(funct_thrs=0.5)

    TFs_families_merged_sets_TFs[set_name] = tmp

# save the TF families dictionary as an object
with open(OUTPUT + "protein_sets/TFs_families_merged_sets_TFs.pickle", 'wb') as f:
    pickle.dump(TFs_families_merged_sets_TFs, f)

###############################################
# GET COMMON PROTEIN FAMILIES IN HUMAN PROTEOME
###############################################

# in addition to the curated TF list, use the Top10 families in the human proteome to compare but exclude families
# that where already analysed above as TF families
families = [i[0] for i in proteome_families_counts if interpro_mapping[i[0]] not in TFs_families.keys()]
protein_families = {}
for fam in families[:top]:
    # gather the genes that are annotated to each family
    genes = human_proteome_interpro.loc[human_proteome_interpro["InterPro"].apply(lambda x: fam in x),
                                        "uniprot_ACC"].tolist()
    protein_families[interpro_mapping[fam]] = genes

# investigate the overlap between different families
plt.figure(figsize=(30, 0.7 * len(protein_families)))
keys = [i for i in protein_families.keys()]
values = [set(protein_families[i]) for i in keys]
a = supervenn(values, keys, sets_ordering='minimize gaps', side_plots=False,
              min_width_for_annotation=20, fontsize=fontsizes[1])
a.axes["main"].set_ylabel("Sets", size=fontsizes[1])
a.axes["main"].set_xlabel("Proteins", size=fontsizes[1])
plt.savefig(OUTPUT + f"protein_sets/protein_families_{top}_before_merge.png")
plt.close()

protein_families_merg, protein_families_ovlp, protein_families_merg_names = mss.find_similiar_sets(protein_families,
                                                                                                   outside_intersect=0)
with open(OUTPUT + f"protein_sets/protein_families_{top}_mergedfam.txt", "w") as f:
    for i, j in enumerate(protein_families_merg):
        f.write(f"{j} were merged into new family: '{protein_families_merg_names[i]}'\n")

protein_families_merged = mss.merge_similar_sets(protein_families, protein_families_merg,
                                                 protein_families_ovlp, protein_families_merg_names)

plt.figure(figsize=(30, 0.7 * len(TFs_families_merged)))
keys = [i for i in protein_families_merged.keys()]
values = [set(protein_families_merged[i]) for i in keys]
a = supervenn(values, keys, sets_ordering='minimize gaps', side_plots=False, min_width_for_annotation=20,
              fontsize=fontsizes[1])
a.axes["main"].set_ylabel("Sets", size=fontsizes[1])
a.axes["main"].set_xlabel("Proteins", size=fontsizes[1])
plt.savefig(OUTPUT + f"protein_sets/protein_families_{top}_after_merge.png")
plt.close()

protein_families_merged_sets = {}
protein_families_merged["curated_TFs"] = []
for (set_name, genes) in protein_families_merged.items():
    print(">>>" + set_name)
    if set_name == "curated_TFs":
        tmp = ProteinSet(TFs_curated, name=set_name)
    else:
        genes_df = mapping_table.convert_id(genes, id_from="uniprot_ACC", id_to="all", retain_nonmap=True)
        tmp = ProteinSet(genes_df, name=set_name)

    tmp.add_phosphosites(phosphositeplus, gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
    tmp.add_regulationannot(phosphositeplus_regulatory, gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
    tmp.add_functionalscore(funct_score_Ochoa, gene_id_data="uniprot", gene_id_members="uniprot_ACC")
    tmp.get_highlyfunctionalsites(funct_thrs=0.5)

    protein_families_merged_sets[set_name] = tmp

# save the TF families dictionary as an object
with open(OUTPUT + "protein_sets/protein_families_merged_sets.pickle", 'wb') as f:
    pickle.dump(protein_families_merged_sets, f)
