import argparse
import pandas as pd

parser = argparse.ArgumentParser(description="Included TF resources")
parser.add_argument("--resource",
                    choices=["Lambert", "Dorothea", "Trrust", "all"],
                    type=str,
                    help="Resources used for curating a list of TFs")

args = parser.parse_args()
if args.resource is None:
    resource = "all"
else:
    resource = args.resource

OUTPUT = "../output/"
if resource != "all":
    OUTPUT += resource + "/"

TFs_curated = pd.read_csv(OUTPUT + "0_curated_TFs.csv")

# load the InterPro families with the protein counts
InterPro_families = pd.read_csv("../output/" + "0_InterPro_human_families_20210309.csv")
interpro_mapping = {acc: InterPro_families.at[i, "name"]
                    for i, acc in enumerate(InterPro_families["accession"])}

# load the human proteome
human_proteome = pd.read_csv("../output/" + "0_Uniprot_humanproteome_Interpro_20210309.tsv",
                             delimiter="\t",
                             usecols=["Entry", "Protein names", "Gene names", "Cross-reference (InterPro)"])
human_proteome.columns = ['uniprot_ACC', 'protein_names', 'uniprot_entryname', 'InterPro']

# gene names contains several gene names ("Name1 Name2"), just keep the first one, this should be the standard one
# the proteome contains some putative proteins that do not have a gene name (nan) even though they are reviewed
tmp = human_proteome.loc[~human_proteome["uniprot_entryname"].isna(), "uniprot_entryname"].apply(lambda x: x.split(" ")[0])
human_proteome.loc[~human_proteome["uniprot_entryname"].isna(), "uniprot_entryname"] = tmp
# only consider proteins that have interpro annotation
human_proteome_interpro = human_proteome.loc[~human_proteome["InterPro"].isna()]

# add the interpro annotation for the list of curated TFs
TFs_curated = pd.merge(TFs_curated, human_proteome_interpro[["uniprot_ACC", "InterPro"]], how="left", on="uniprot_ACC")
TFs_curated = TFs_curated.fillna("")

# only keep interpro IDs that correspond to families or superfamilies, don't keep etc. domains
TFs_curated["InterPro_fam_list"] = [[j + ";" for j in i.split(";")[:-1] if j in interpro_mapping.keys()]
                                    for i in TFs_curated["InterPro"]]

# add another column with the translation of the interpro domains
tmp = [[interpro_mapping[j.strip(";")] for j in i] for i in TFs_curated["InterPro_fam_list"]]
tmp = [";".join(i) for i in tmp]
TFs_curated["InterPro_fam_name"] = tmp

# export dataframe, keep only the interpro columns that correspond to families or superfamilies
TFs_curated["InterPro_fam"] = ["".join(i) for i in TFs_curated["InterPro_fam_list"]]

export_cols = [i for i in TFs_curated.columns if i not in ["InterPro_fam_list", "InterPro"]]
TFs_curated[export_cols].to_csv(OUTPUT + "2_TF_interpro_family_annotation.csv", index=False)
