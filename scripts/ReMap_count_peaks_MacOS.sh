#!/bin/bash

wget http://remap.univ-amu.fr/storage/remap2020/hg38/MACS2/remap2020_nr_macs2_hg38_v1_0.bed.gz

echo "Npeaks	TF" > input/chromatin_binding/ReMap2020peaksFiltered2.tsv

zcat < remap2020_nr_macs2_hg38_v1_0.bed.gz | awk '{print $4}' | sed  's/:.*$//g' | grep -v -e "-" -e "phosph" | LC_ALL=C sort --parallel=8 | LC_ALL=C uniq -c | sed 's/^\s*//g' | sed 's/^[[:space:]]*//g' | sed 's/ /\t/g' >> input/chromatin_binding/ReMap2020peaksFiltered2.tsv

rm remap2020_nr_macs2_hg38_v1_0.bed.gz