import argparse
import pandas as pd
from datetime import date
import requests

parser = argparse.ArgumentParser(description="Included TF resources")
parser.add_argument("--resource",
                    choices=["Lambert", "Dorothea", "Trrust", "all"],
                    type=str,
                    help="Resources used for curating a list of TFs")

args = parser.parse_args()
if args.resource is None:
    resource = "all"
else:
    resource = args.resource

OUTPUT = "../output/"
if resource != "all":
    OUTPUT += resource + "/"

DATE = date.today().strftime("%Y%m%d")

TFs_curated = pd.read_csv(OUTPUT + "0_curated_TFs.csv")

genes = TFs_curated["HGNC_symbol"].unique().tolist()
hits = {}
json_key = ['esearchresult', 'count']
for i, gene in enumerate(genes):
    if i % 100 == 0:
        print(f"requesting gene number {i}")
    url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={gene}[" \
          f"Title%2FAbstract]&retmode=json&api_key=ddfb0bfd927f4e0a99679179c197d80d9a08"
    res = requests.get(url)
    hits[gene] = res.json()[json_key[0]][json_key[1]]

# check that all genes have been queried
non_queried_genes = set(genes).difference(hits.keys())
if len(non_queried_genes) > 0:
    print("Not all genes were queried. Check file `pubmed_non_queried_genes.txt`")
    with open(OUTPUT + 'pubmed_non_queried_genes.txt', 'w') as f:
        for item in non_queried_genes:
            f.write(f"{item}\n")

TFs_pubmedhits = pd.DataFrame.from_dict(hits, orient='index').reset_index()
TFs_pubmedhits.columns = ["HGNC_symbol", "pubmed_count"]
TFs_pubmedhits["pubmed_count"] = pd.to_numeric(TFs_pubmedhits["pubmed_count"])
TFs_pubmedhits = TFs_pubmedhits.sort_values(by="pubmed_count", ascending=False).reset_index(drop=True)
TFs_pubmedhits.to_csv(OUTPUT + f"1_TF_pubmedcount_tit-abstr_{DATE}.csv", index=False)
