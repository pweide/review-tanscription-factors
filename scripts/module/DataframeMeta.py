''' DataframeMeta.py
This module helps to define store metadata together with a dataframe.
'''

import pandas as pd

class DataframeMeta:
    '''
    The SetDataframe class eases the storage of metadata with a dataframe.
    
    Parameters
    ----------
    df : pandas.DataFrame
        A dataframe containing rows and columns with specific information.
    meta : dict
        Dictionary that stores the meta information about the dataframe with meaningfull keys and values. Key's can be chosen freely by the user.

    Attributes
    ----------
    '''
    
    def __init__(self, dataframe, metadata):
        '''
        Stores a dataframe alongside with meta information about the dataframe.
        
        Parameters
        ----------
        dataframe : pandas.DataFrame
            A dataframe containing rows and columns with specific information.
        metadata : dict
            Dictionary that stores the meta information about the dataframe with meaningfull keys and values. Key's can be chosen freely by the user
        '''
        
        self.df = dataframe
        self.meta = metadata
        
    def print_metadata(self):
        '''
        Prints each key and value pair of `self.meta` per line.
        '''
        for key, val in self.meta.items():
            print(key, ":", val)
            
    def get_metadata(self, key):
        '''
        Returns the value for a given key from `self.meta`.
        '''
        return(self.meta[key])