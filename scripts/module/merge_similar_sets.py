import numpy as np


def find_parentset(family, list_families, sets, outside_intersect=5):
    parent_families = []
    protein_fam = sets[family]
    for i in list_families:
        protein_other = sets[i]
        unique = len(set(protein_fam).difference(protein_other))
        if unique <= outside_intersect and len(set(protein_fam)) > outside_intersect:
            parent_families += [i]
    overlapping_set = []
    if len(parent_families) > 0:
        for j in parent_families:
            overlapping_set += [[family, j]]

    return overlapping_set


def find_similiar_sets(sets, outside_intersect=5, exclude_sets=[]):
    # get list containing lists with first family being a subset of the second family
    overlapping_sets = []
    tmp_sets = [i for i in sets.keys() if i not in exclude_sets]

    for fam in tmp_sets:
        other_fams = [i for i in tmp_sets if i != fam]
        overlapping_set = find_parentset(fam, other_fams, sets=sets, outside_intersect=outside_intersect)
        overlapping_sets += overlapping_set
    # merge all the binary subsets into big subsets
    # then merge all pairs that share families.
    # e.g. if set 1 is mostly contained in set 2 and set 2 is mostly contained in set 3, merge all three
    # e.g. if set 1 ist contained in set 2 and set 3 in set 2, merge all three
    # e.g. if set 1 is contained in set 2 and also contained in set 3, but set 2 is not contained in 3
    # -> 1,2,3 will still be merged NOT IDEAL

    # treat the binary list as a edge list for a graph -> build a graph -> extract connected components
    # -> these are my merged families
    import networkx as nx
    g = nx.from_edgelist(overlapping_sets)
    merging_sets = list(list(i) for i in nx.connected_components(g))

    # determine the largest set in each component, choose this one as the name for the set
    merge_names = []
    for comp in merging_sets:
        sizes = [len(sets[i]) for i in comp]
        largest_set = comp[np.argsort(sizes)[-1]]
        merge_names += [largest_set + "*"]

    return merging_sets, overlapping_sets, merge_names

def find_similiar_sets_name(sets, exclude_sets=[]):
    # merge sets if they are named very similiar
    # one set is a family and the other set is the superfamily
    # e.g. "nuclear hormone receptor" and "nuclear hormone receptor-like domain superfamily"

    # get list containing lists with first family being a subset of the second family
    overlapping_sets = []
    tmp_sets = [i for i in sets.keys() if i not in exclude_sets]

    for fam in tmp_sets:
        # remove the last word in the name e.g. superfamily or family
        # also remove -like and domain to get the meaningful part of the family name
        fam_name = fam.replace("family", "").replace("super", "")
        fam_name = fam_name.replace("-like", "").replace("domain", "").strip()
        other_fams = [i for i in tmp_sets if i != fam]

        same_families = []
        protein_fam = sets[fam]
        for i in other_fams:
            other_fam_name = i.replace("family", "").replace("super", "")
            other_fam_name = other_fam_name.replace("-like", "").replace("domain", "").strip()
            if other_fam_name == fam_name:
                same_families += [i]

        overlapping_set = []
        if len(same_families) > 0:
            for j in same_families:
                overlapping_set += [[fam, j]]

        overlapping_sets += overlapping_set

    # merge all the binary subsets into big subsets
    # then merge all pairs that share families.
    # e.g. if set 1 is mostly contained in set 2 and set 2 is mostly contained in set 3, merge all three
    # e.g. if set 1 ist contained in set 2 and set 3 in set 2, merge all three
    # e.g. if set 1 is contained in set 2 and also contained in set 3, but set 2 is not contained in 3
    # -> 1,2,3 will still be merged NOT IDEAL

    # treat the binary list as a edge list for a graph -> build a graph -> extract connected components
    # -> these are my merged families
    import networkx as nx
    g = nx.from_edgelist(overlapping_sets)
    merging_sets = list(list(i) for i in nx.connected_components(g))

    # determine the largest set in each component, choose this one as the name for the set
    merge_names = []
    for comp in merging_sets:
        sizes = [len(sets[i]) for i in comp]
        largest_set = comp[np.argsort(sizes)[-1]]
        merge_names += [largest_set + "*"]

    return merging_sets, overlapping_sets, merge_names

# get the new merged protein sets
def merge_similar_sets(all_sets, merging_sets, merging_sets_names, exclude_sets=[]):
    overlapping_sets_all = set([j for i in merging_sets for j in i])
    unique_sets = [i for i in all_sets.keys() if i not in exclude_sets and i not in overlapping_sets_all]

    merged_sets = {}
    for i, set_names in enumerate(merging_sets):
        union = [all_sets[i] for i in set_names]
        merged_sets[merging_sets_names[i]] = set().union(*union)

    for name in unique_sets:
        merged_sets[name] = all_sets[name]

    return merged_sets


