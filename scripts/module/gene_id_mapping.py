import pandas as pd
import numpy as np


class GeneIdMapping:
    """
        The GeneIdMapping class loads a data frame containing the mapping of all human genes between several common
        gene identifiers obtained from Ensembl's BioMart service using the GRCh38.p13 dataset.
        Given a string of a single gene or a list of genes, they can be quickly mapped from one identifier to another
        one.

        Attributes
        ----------
        mapping_table : pandas.DataFrame
            A dataframe containing the ID mappings for all human genes.

    """

    def __init__(self, filename):
        """
        Load a data frame containing the mapping of all human genes between several common
        gene identifiers obtained from Ensembl's BioMart service using the GRCh38.p13 dataset.
        """
        self.mapping_table = pd.read_csv(filename)
        self.mapping_table.columns = ['ensembl_ID', 'HGNC_ID', 'HGNC_symbol', 'uniprot_ACC', 'uniprot_entryname']

    def available_ids(self):
        """ Returns a list of available IDs."""
        return self.mapping_table.columns.tolist()

    def convert_id(self, ids, id_from, id_to, retain_nonmap=False, drop_na=False):
        """
        Given a string of a single gene or a list of genes, they can be quickly mapped from one identifier to another
        one. Several identifiers can be passed for mapping to.

        Parameters
        ----------
        ids : str or list
            A string of a single gene or a list of genes in the identifier format of `id_from`.
        id_from : str
            The identifier in which the passed genes are encoded in.
        id_to: str
            A string of a single identifier or a list of identifiers to which the genes should be passed. You can
            also pass `all` then the genes are mapped to all identifiers in the table.
        drop_na : bool, optional
            Should rows were all mapping IDs contain NaNs be dropped? Default is False.

        Returns
        -------
        pd.DataFrame
            A data frame that contains one row per gene and a column for each identifier.
            Rows were all values in the id_to columns were NaN were dropped.
        """

        if id_from not in self.available_ids():
            raise (KeyError(f"The identifier {id_from} is not present in the mapping table. Please choose one "
                            f"of the following: {self.available_ids()}"))
        if type(id_to) == str:
            if id_to == "all":
                mapping_ids_to = [i for i in self.available_ids() if i != id_from]
            elif id_to not in self.available_ids():
                raise (KeyError(f"The identifier {id_to} is not present in the mapping table. Please choose one of "
                                f"the "
                                f"following: {self.available_ids()}"))
            else:
                mapping_ids_to = [id_to]
        else:
            missing_ids = set(id_to).difference(self.available_ids())
            mapping_ids_to = [i for i in id_to if i not in missing_ids]
            if len(missing_ids) > 0:
                if len(missing_ids) == len(id_to):
                    raise (KeyError(f"None of the id_to identifiers are present in the mapping table. Please choose "
                                    f"one of the following: {self.available_ids()}"))
                else:
                    print(f"Not all identifiers were found in the mapping table. The remaining ones "
                          f"will be use for mapping.\nFollowing mapping identifier are not in the mapping "
                          f"table: {missing_ids}")

        if type(ids) == str:
            ids = [ids]
        # retrieve all unique mappings, but drop na mappings and return them as a table
        non_match_ids = set(ids).difference(self.mapping_table[id_from])
        if len(non_match_ids) > 0:
            if len(non_match_ids) == len(ids):
                print(f"None of your query ids could not be found in {id_from}. Returning empty pd.DataFrame.")
                return pd.DataFrame()
            else:
                if retain_nonmap:
                    print(f"Not all query ids could be found in {id_from}. The non mapping query ids "
                          f"will be retained in the table but will have no mapping ids in the other columns.\n"
                          f"Following query ids could not be found : {non_match_ids}")
                else:
                    print(f"Not all query ids could be found in {id_from}, continuing with the remaining ones.\n"
                          f"Following query ids could not be found : {non_match_ids}")

        if retain_nonmap:
            mapped_ids = pd.DataFrame(ids, columns=[id_from])
            mapped_ids = mapped_ids.merge(self.mapping_table[[id_from] + mapping_ids_to], on=id_from, how="left")
        else:
            mapped_ids = self.mapping_table.loc[self.mapping_table[id_from].isin(ids),
                                                [id_from] + mapping_ids_to].copy()
        if drop_na:
            mapped_ids = mapped_ids.dropna(subset=mapping_ids_to, how="all")

        mapped_ids = mapped_ids.drop_duplicates()

        if id_from == mapping_ids_to[0]:
            mapped_ids = mapped_ids.iloc[:, 0]

        if id_to == "all":
            # for some reason often times there are two rows where the same genes has a uniprot_ACC in one row but
            # not in the other
            # in both rows a uniprot_entryname exists
            # I will remove rows that contain an uniprot_entryname  but no uniprot_ACC given there is another row
            # that contains the same IDs but with a uniprot_entryname
            index_cols = np.array([i for i in mapped_ids.columns if i != 'uniprot_ACC'])
            dropped = []
            uniqID = {i: hash(tuple(mapped_ids.iloc[i][index_cols].values)) for i in range(mapped_ids.shape[0])}
            for i in range(mapped_ids.shape[0]):
                row_i = uniqID[i]
                for j in range(mapped_ids.shape[0]):
                    if i != j:
                        row_j = uniqID[j]
                        if row_j == row_i:
                            # decision
                            if mapped_ids.iloc[i]['uniprot_ACC'] is np.nan:
                                dropped.append(i)
                            elif mapped_ids.iloc[j]['uniprot_ACC'] is np.nan:
                                dropped.append(j)

            mapped_ids = mapped_ids.loc[~mapped_ids.index.isin(dropped)]

        return mapped_ids
