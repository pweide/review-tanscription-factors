""" ProteinSet.py
This module helps to define ProteinSets and add phosphorylation, regulatory annotation and
expression data for each member of the set. The main purpose of the module is to get counts about the number of
phosphosites, protein abundances and several other metrics for each member of the set.
"""
import pandas as pd
import numpy as np
from collections import Counter
import re

from module.DataframeMeta import DataframeMeta

print("ProteinSet is loaded")


class ProteinSet:
    """
    The ProteinSet class contains a list of genes/proteins defined as a set (e.g. belonging to the same protein
    family).
    ProteinSet functions can add several metrics to the members of the set, e.g. phosphorylation or expression
    information.
    
    Parameters
    ----------
    genes : dict
        A dictionary containing as key the gene identifier type (e.g. "HGNC") and as value a list of gene names.
    name : str
        Name of the set.
    base_gene_id : str, optional
        The gene identifier that is used as the gene identifier to map counts to. This way there is a unique mapping
        of counts to a protein member, because ambivalences in gene ID mapping are resolved.

    Attributes
    ----------
    members : pandas.DataFrame
        A dataframe containing the members of a protein family. Columns are mapping gene identifier types (e.g.
        "HGNC_symbol") with each row containing the given identifiers for one member.
    name : str
        Name of the ProteinSet.
    interaction : DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with all protein interactions involving any of the set
        members. Each row is one interaction. In the `meta` attribute a dictionary with different meta information
        about the dataframe is stored.
    phospho: DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with all phosphorylation sites known for any of the
        set members. Each row contains one phosphorylation site with gene and position annotated. In the `meta`
        attribute a dictionary with different meta information about the dataframe is stored.
    regul_annot : DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with all phosphorylation sites with functional
        annotation known for any of the set members. Each row contains one phosphorylation site with gene, position and
        function annotated. In the `meta` attribute a dictionary with different meta information about the dataframe
        is stored.
    functional_score : DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with all phosphorylation sites with a functional
        score known for any of the set members. Each row contains one phosphorylation site with gene, position and
        functional score annotated. In the `meta` attribute a dictionary with different meta information about the
        dataframe is stored.
    protein_expression : DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with the protein expression value of any of the set
        members. Each row contains one gene expression value per member. In the `meta` attribute a dictionary
        with different meta information about the dataframe is stored.
    regulons: DataframeMeta object
        The object stores a dataframe (in the `df` attribute) with the target genes of any of the set members. Each
        row contains one target gene for one set member. In the `meta` attribute a dictionary
        with different meta information about the dataframe is stored.
    counts : pandas.DataFrame
        A dataframe that stores the counts of different metrics for each protein member. Members are given using the
        `gene_id_base` identifier. This way there is a unique mapping of counts to a protein member,
        because ambivalences in gene ID mapping are resolved.
    gene_id_base : str
        The gene identifier that is used as the gene identifier to map counts to. This way there is a unique mapping
        of counts to a protein member, because ambivalences in gene ID mapping are resolved.
    """

    def __init__(self, genes, name, base_gene_id="HGNC_symbol"):
        """
        Assign the family name and list of genes belonging to the family.
        
        Parameters
        ----------
        genes : pandas.DataFrame
            A dataframe containing the members of a protein family. Columns are the gene identifier type (e.g. 
            "HGNC") with each row containing the given identifiers for one gene.
        name : str
            A name for the protein set.
        base_gene_id : str, optional
            Gene ID that is used as a base for all operations on the set dataframes unless specified otherwise in 
            the individual class functions. Default is `uniprot_entryname`.
        """
        self.members = genes
        self.name = name
        self.interaction = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.phosphorylation = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.regulation_annot = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.functional_score = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.protein_expression = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.regulons = DataframeMeta(dataframe=pd.DataFrame(), metadata={})
        self.counts = genes
        self.gene_id_base = base_gene_id
        self.init_count_table()

    def init_count_table(self):
        self.counts = self.counts[[self.gene_id_base]].drop_duplicates()

    def get_members(self, gene_id="gene_symbol"):
        """
        Returns a list of protein family members in the given gene identifier type.
        """
        return self.members[gene_id].unique().tolist()

    def print_basic_stats_resource(self, gene_id_members="base"):
        if gene_id_members == "base":
            gene_id_members = self.gene_id_base

        print("The set '{}' contains {} members (gene_id={}).".format(self.name,
                                                                      len(self.members[gene_id_members].unique()),
                                                                      gene_id_members))

    def convert_id_count_table(self, count_dict, gene_id, count_col):

        count_df = pd.DataFrame.from_dict(count_dict, orient="index").reset_index()
        count_df.columns = [gene_id, count_col]

        if gene_id != self.gene_id_base:
            count_df = count_df.merge(
                self.members[[gene_id, self.gene_id_base]].drop_duplicates(), on=gene_id)

        # map the counts back to the base ID which is used for all countings. If several of the merge IDs map to one
        # base ID the counts are summed
        # to retain NaNs and avoid that they are set to 0 while summing, set min_count=1
        count_df = count_df[[self.gene_id_base, count_col]]
        count_df = count_df.groupby(by=self.gene_id_base).sum(min_count=1).reset_index()
        count_df.columns = [self.gene_id_base, count_col]

        return count_df

    def add_column_counts(self, df, col_name, merge_on="base"):

        if col_name in self.counts.columns:
            print(
                f"The protein set already has the column '{col_name}'."
                f"The column will be updated and the old entries will be deleted.")
            del self.counts[col_name]

        if merge_on == "base":
            merge_on = self.gene_id_base

        self.counts = pd.merge(self.counts, df, how="left", on=merge_on)

    def add_counts(self, count_table, count_col, gene_id_data="HGNC_symbol",
                   gene_id_members="HGNC_symbol"):
        """
        Merges the number of publications stored in the `publications` dataframe with the
        `self.counts` dataframe for each member gene based on the passed gene identifiers.

        Parameters
        ----------
        count_table : pandas.DataFrame
            Dataframe containing one gene per row with a column indicating the gene identifier and second column
            indicating the counts for a given metric.
        count_col : str
            Name of the column in `count_table` containing the counts.
        gene_id_data : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `publications` dataframe.
        gene_id_members : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `self.members` dataframe.
        """
        # make sure both the publication dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in count_table.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `count_table` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        self.print_basic_stats_resource(gene_id_members)

        # rename column such that the merging ID is the same between members and passed resource
        counts = count_table.rename(columns={gene_id_data: gene_id_members})

        if gene_id_members != self.gene_id_base:
            counts = counts.merge(
                self.members[[gene_id_members, self.gene_id_base]].drop_duplicates(), on=gene_id_members)

        self.add_column_counts(counts[[self.gene_id_base, count_col]], count_col)

        print(f"DONE! Column '{count_col}' was added to the count table.")

    def add_target_genes(self, regulons, count_col, gene_id_data="HGNC_symbol", gene_id_members="HGNC_symbol"):
        """

        Parameters
        ----------
        regulons
        gene_id_data
        gene_id_members

        Returns
        -------

        """
        # make sure both the publication dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in regulons.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `regulons` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        self.print_basic_stats_resource(gene_id_members)

        print(
            "The regulon dataset contains {} unique entries of target gene information for {} unique "
            "TFs.".format(regulons.shape, len(set(regulons[gene_id_data]))))

        # subset the phosphorylation information to only the members in the protein set
        gene_i = set(self.members[gene_id_members]).intersection(set(regulons[gene_id_data]))
        self.regulons.df = regulons[regulons[gene_id_data].isin(gene_i)]
        self.regulons.df = self.regulons.df.rename(columns={gene_id_data: gene_id_members})
        self.regulons.meta = {"merge_gene_id_members": "no-merge",
                              "gene_id": gene_id_members}

        print("***Of {} {} members {} have target gene information in the  "
              "regulon dataset.".format(len(self.members[gene_id_members].unique()), self.name,
                                        len(gene_i)))

        # make a dictionary with the number of phosphorylation entries for each protein
        regulons_counts = self.regulons.df[gene_id_members].value_counts().to_dict()

        regulons_counts = pd.DataFrame.from_dict(regulons_counts, orient="index").reset_index()
        regulons_counts.columns = [gene_id_members, count_col]

        if gene_id_members != self.gene_id_base:
            regulons_counts = regulons_counts.merge(
                self.members[[gene_id_members, self.gene_id_base]].drop_duplicates(), on=gene_id_members)

        # map the counts back to the base ID which is used for all countings. If several of the merge IDs map to one
        # base ID the counts are summed
        # to retain NaNs and avoid that they are set to 0 while summing, set min_count=1
        regulons_counts = regulons_counts[[self.gene_id_base, count_col]]
        regulons_counts = regulons_counts.groupby(by=self.gene_id_base).sum(min_count=1).reset_index()
        regulons_counts.columns = [self.gene_id_base, count_col]

        self.add_column_counts(regulons_counts, count_col)

        print(f"DONE! Column '{count_col}' was added to the count table.")

    def add_interaction_partners(self, interaction, count_col="Intact_interactions", gene_id_data="uniprot",
                                 gene_id_members="uniprot_entryname"):
        """

        Parameters
        ----------
        interaction
        gene_id_data : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `interaction` dataframe.
        gene_id_members : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `self.members` dataframe.
        """
        gene_id_data_a = "InteractorA_" + gene_id_data
        gene_id_data_b = "InteractorB_" + gene_id_data

        not_in_interaction = any([gene_id_data_a not in interaction.columns,
                                  gene_id_data_b not in interaction.columns])

        # make sure both the phosphorylation dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or not_in_interaction:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `phospho` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        self.print_basic_stats_resource(gene_id_members)

        print(
            "The interaction dataset contains {} unique entries of interaction information for {} unique interactors "
            "A and {} unique interactors B".format(interaction.shape[0],
                                                   len(set(interaction[gene_id_data_a])),
                                                   len(set(interaction[gene_id_data_b]))))

        # subset the phosphorylation information to only the members in the protein set
        interactors = set(interaction[gene_id_data_a]).union(interaction[gene_id_data_b])
        gene_i = set(self.members[gene_id_members]).intersection(interactors)

        self.interaction.df = interaction.loc[interaction[gene_id_data_a].isin(gene_i) |
                                              interaction[gene_id_data_b].isin(gene_i)]
        self.interaction.meta = {"merge_gene_id_members": "no-merge"}

        print("***Of {} {} members {} have interaction information (total of {} interactions) in the "
              "phosphorylation dataset.".format(len(self.members[gene_id_members].unique()), self.name,
                                                len(gene_i), self.interaction.df.shape[0]))

        # make a dictionary with the number of interaction entries for each TF per passed ID
        def get_interactors(lst):
            """count the number of interactors for each gene"""
            a = interaction.loc[interaction[gene_id_data_a].isin(lst) | interaction[gene_id_data_b].isin(lst)]
            return a.shape[0]

        interaction_counts = {i: get_interactors([i]) if i in gene_i else np.nan
                              for i in self.members[gene_id_members].unique()}

        interaction_counts = pd.DataFrame.from_dict(interaction_counts, orient="index").reset_index()
        interaction_counts.columns = [gene_id_members, count_col]

        if gene_id_members == self.gene_id_base:
            interaction_counts = interaction_counts.merge(
                self.members[[self.gene_id_base]].drop_duplicates(), on=self.gene_id_base)
        else:
            interaction_counts = interaction_counts.merge(
                self.members[[gene_id_members, self.gene_id_base]].drop_duplicates(), on=gene_id_members)

        # map the counts back to the base ID which is used for all countings. If several of the merge IDs map to one
        # base ID the counts are summed
        # to retain NaNs and avoid that they are set to 0 while summing, set min_count=1
        interaction_counts = interaction_counts[[self.gene_id_base, count_col]]
        interaction_counts = interaction_counts.groupby(by=self.gene_id_base).sum(min_count=1).reset_index()
        interaction_counts.columns = [self.gene_id_base, count_col]

        self.add_column_counts(interaction_counts, count_col)

        print(f"DONE! Column '{count_col}' was added to the count table.")

    def add_phosphosites(self, phospho_input, count_col="phosphorylation_count", gene_id_data="GENE",
                         gene_id_members="uniprot_entryname"):
        """
        Subsets the phosphorylation datasets to genes in the protein family. Retrieves the number of phosphosites
        annotated for each gene.
        The subsetted dataframe will be stored in the `self.phosphorylation` object. `gene_id_members` will be used
        to denote the type of gene identifier.
        
        Parameters
        ----------
        phospho_input : pandas.DataFrame
            Dataframe containing one phosphorylation per row with gene and position annotated.
        gene_id_data : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `phospho` dataframe.
        gene_id_members : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `self.members` dataframe.
        """
        # make sure both the phosphorylation dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in phospho_input.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `phospho_input` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        self.print_basic_stats_resource(gene_id_members)

        # remove rows where `gene_id_data` is NaN
        phospho = phospho_input[~phospho_input[gene_id_data].isna()].copy()

        print(
            "The phosphorylation dataset contains {} unique entries of phoshorylation information for {} unique "
            "genes.".format(phospho.shape[0], len(set(phospho[gene_id_data]))))

        # subset the phosphorylation information to only the members in the protein set
        gene_i = set(self.members[gene_id_members]).intersection(set(phospho[gene_id_data]))
        self.phosphorylation.df = phospho[phospho[gene_id_data].isin(gene_i)]
        self.phosphorylation.df = self.phosphorylation.df.rename(columns={gene_id_data: gene_id_members})
        self.phosphorylation.meta = {"merge_gene_id_members": "no-merge",
                                     "gene_id": gene_id_members}

        print("***Of {} {} members {} have phosphorylation information in the  "
              "phosphorylation dataset.".format(len(self.members[gene_id_members].unique()), self.name,
                                                len(gene_i)))

        # make a dictionary with the number of phosphorylation entries for each protein
        phospho_counts = self.phosphorylation.df[gene_id_members].value_counts().to_dict()

        phospho_counts = pd.DataFrame.from_dict(phospho_counts, orient="index").reset_index()
        phospho_counts.columns = [gene_id_members, count_col]

        if gene_id_members != self.gene_id_base:
            phospho_counts = phospho_counts.merge(
                self.members[[gene_id_members, self.gene_id_base]].drop_duplicates(), on=gene_id_members)

        # map the counts back to the base ID which is used for all countings. If several of the merge IDs map to one
        # base ID the counts are summed
        # to retain NaNs and avoid that they are set to 0 while summing, set min_count=1
        phospho_counts = phospho_counts[[self.gene_id_base, count_col]]
        phospho_counts = phospho_counts.groupby(by=self.gene_id_base).sum(min_count=1).reset_index()
        phospho_counts.columns = [self.gene_id_base, count_col]

        self.add_column_counts(phospho_counts, count_col)

        print(f"DONE! Column '{count_col}' was added to the count table.")

    def add_regulationannot(self, regul_annot, count_col="regulation_count",
                            gene_id_data="ACC_ID", gene_id_members="uniprot_ACC"):
        """
        Adds info of regulation annotation for each phosphosite present in the dataframe of the
        `self.phosphorylation` object. Retrieves the number of phosphosites with regulatory information for each gene.
        The merged dataframe will be stored as `self.regulation_annot.df`. `gene_id_members` will be used to denote
        the type of gene identifier.
        
        Parameters
        ----------
        regul_annot : pandas.DataFrame
            dataframe containing one phosphorylation per row with gene, position and regulatory effect annotated.
        gene_id_data : str
            type of gene identifier that should be used for subsetting. The identifier should be present as a column name in the `regul_annot` dataframe.
        gene_id_members : str
            type of gene identifier that should be used for subsetting. The identifier should be present as a column name in the `self.members` dataframe.
        """
        # make sure both the regulatory annotation dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in regul_annot.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `regul_annot` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        print("The set '{}' contains {} members (gene_id={}).".format(self.name,
                                                                      len(self.members[gene_id_members].unique()),
                                                                      gene_id_members))

        print("The regulatory dataset contains regulatory annotation for {} phosphosites (rows) in {} unique "
              "genes.".format(regul_annot.shape[0], len(set(regul_annot[gene_id_data]))))

        # intersection of genes in set and with regulatory annotation
        gene_i = set(self.members[gene_id_members]).intersection(set(regul_annot[gene_id_data]))

        # merge the regulatory dataframe with the phosphorylation dataframe, do a left merge meaning only entries
        # already in the dataframe of the `self.phosphorylation` object should be kept
        phospho_id = self.phosphorylation.meta["gene_id"]

        self.regulation_annot.df = self.phosphorylation.df.merge(regul_annot[[gene_id_data, "MOD_RSD", "ON_FUNCTION",
                                                                              "ON_PROCESS", "ON_PROT_INTERACT",
                                                                              "ON_OTHER_INTERACT"]],
                                                                 how="left",
                                                                 left_on=[phospho_id, "MOD_RSD"],
                                                                 right_on=[gene_id_data, "MOD_RSD"])

        self.regulation_annot.meta = {"merge_gene_id_members": phospho_id,
                                      "merge_phospho_df": [phospho_id, "MOD_RSD"],
                                      "gene_id": phospho_id}

        # count the number of annotated processes/functions
        self.regulation_annot.df["ON_FUNCTION_count"] = self.regulation_annot.df.loc[:, "ON_FUNCTION"].str.split(
            ";").str.len()
        self.regulation_annot.df["ON_PROCESS_count"] = self.regulation_annot.df.loc[:, "ON_PROCESS"].str.split(
            ";").str.len()

        # count number of functional phosphosites (phosphosites that have information about their effect
        # (e.g. process, function, interaction))

        n_funct_phosphosites = \
            self.regulation_annot.df.loc[self.regulation_annot.df[gene_id_members].isin(gene_i), :].shape[0]

        print(
            "***Of {} {} members {} have any regulatory information (total of {} phosphosites) in the regulation "
            "dataset.".format(len(self.members[gene_id_members].unique()),
                              self.name, len(gene_i), n_funct_phosphosites))

        # get the most commonly annotated functions
        functs = self.regulation_annot.df.loc[~self.regulation_annot.df["ON_FUNCTION"].isna(),
                                              "ON_FUNCTION"].str.split("; ")
        functions = Counter([j for i in functs for j in i]).most_common()

        self.regulation_annot_functions = pd.DataFrame(functions, columns=["Function", "Count"])

        print("Most common annotated regulatory ON_FUNCTION are: {}".format(
            "; ".join([i[0] for i in functions[:5]])))

        # get the most commonly annotated processes
        procs = self.regulation_annot.df.loc[~self.regulation_annot.df["ON_PROCESS"].isna(),
                                             "ON_PROCESS"].str.split("; ")

        processes = Counter([j for i in procs for j in i]).most_common()

        self.regulation_annot_functions = pd.DataFrame(processes, columns=["Function", "Count"])
        print("Most common annotated regulatory ON_PROCESS are: {}".format(
            "; ".join([i[0] for i in processes[:5]])))

        # dataframe containing phosphosites with any functional annotation (either function or process - information
        # about interactions are ignored for now)
        regulat_annot_any = self.regulation_annot.df.loc[(~self.regulation_annot.df["ON_FUNCTION"].isna()) |
                                                         (~self.regulation_annot.df["ON_PROCESS"].isna()), :]

        # make a dictionary with the number of phosphorylation entries with any regulatory information for each gene
        # NaN for genes that have no regulatory annotation in the `self.regulation_annot.df` dataset

        # make a dictionary with the number of regulatory entries for each protein
        regul_counts_dict = regulat_annot_any[phospho_id].value_counts().to_dict()
        if regulat_annot_any.shape[0] < 1:
            regul_counts = pd.DataFrame(self.counts[self.gene_id_base].tolist(), columns=[self.gene_id_base])
            regul_counts[count_col] = np.nan
        else:
            regul_counts = self.convert_id_count_table(regul_counts_dict, phospho_id, count_col)
        self.add_column_counts(regul_counts, count_col)

        # for each gene just sum up the number of annotated functions over all phosphosites.
        # make a dictionary with the number of annotated functions for each gene
        # sum will put a 0 if the values entry contains NaN
        sum_function = self.regulation_annot.df.pivot_table(index=phospho_id, values="ON_FUNCTION_count",
                                                            aggfunc=sum)['ON_FUNCTION_count']
        phospho_reg_counts_dict = sum_function.to_dict()
        phospho_reg_counts = self.convert_id_count_table(phospho_reg_counts_dict, phospho_id, "ON_FUNCTIONterms_count")
        self.add_column_counts(phospho_reg_counts, "ON_FUNCTIONterms_count")

        # also just count the number of phosphosites with functional annotation per gene
        a = self.regulation_annot.df.loc[(~self.regulation_annot.df["ON_FUNCTION"].isna()), :]
        if a.shape[0] < 1:
            regul_counts = pd.DataFrame(self.counts[self.gene_id_base].tolist(), columns=[self.gene_id_base])
            regul_counts["ON_FUNCTION_count"] = np.nan
        else:
            regul_counts_dict = a[phospho_id].value_counts().to_dict()
            regul_counts = self.convert_id_count_table(regul_counts_dict, phospho_id, "ON_FUNCTION_count")
        self.add_column_counts(regul_counts, "ON_FUNCTION_count")

        # for each gene just sum up the number of annotated processes over all phosphosites.
        # make a dictionary with the number of annotated processes for each gene
        # sum will put a 0 if the values entry contains NaN
        sum_process = self.regulation_annot.df.pivot_table(index=phospho_id, values="ON_PROCESS_count",
                                                           aggfunc=sum)['ON_PROCESS_count']
        phospho_reg_counts_dict = sum_process.to_dict()
        phospho_reg_counts = self.convert_id_count_table(phospho_reg_counts_dict, phospho_id, "ON_PROCESSterm_count")
        self.add_column_counts(phospho_reg_counts, "ON_PROCESSterm_count")

        # also just count the number of phosphosites with process annotation per gene
        a = self.regulation_annot.df.loc[(~self.regulation_annot.df["ON_PROCESS"].isna()), :]
        if a.shape[0] < 1:
            regul_counts = pd.DataFrame(self.counts[self.gene_id_base].tolist(), columns=[self.gene_id_base])
            regul_counts["ON_PROCESS_count"] = np.nan
        else:
            regul_counts_dict = a[phospho_id].value_counts().to_dict()
            regul_counts = self.convert_id_count_table(regul_counts_dict, phospho_id, "ON_PROCESS_count")
        self.add_column_counts(regul_counts, "ON_PROCESS_count")

        print(f"DONE! Column '{count_col}' was added to the count table.")

    def add_functionalscore(self, funct_score, gene_id_data="uniprot", gene_id_members="uniprot_ACC"):
        """
        Adds functional score for each phosphosite present in dataframe of the `self.phosphorylation` object. Retrieves the number of phosphosites with functional score for each gene.
        The merged dataframe will be stored as `self.regulation_annot.df`. `gene_id_members` will be used to denote the type of gene identifier.
        
        Parameters
        ----------
        funct_score :  pandas.DataFrame
            Dataframe containing one phosphosite per row with gene, position and functional score annotated.
        gene_id_data : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `funct_score` dataframe.
        gene_id_members : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `self.members` dataframe.
        """
        # make sure both the regulatory annotation dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in funct_score.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `funct_score` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        print("The set '{}' contains {} members (gene_id={}).".format(self.name,
                                                                      len(self.members[gene_id_members].unique()),
                                                                      gene_id_members))
        print("The dataset containing functional scores contains {} unique entries of functional scores for "
              "phosphosites on {} unique genes (per uniprot ID).".format(funct_score.shape[0],
                                                                         len(set(funct_score[gene_id_data]))))

        # subset the functional score data to intersection of genes in set and with functional score annotation
        gene_i = set(self.members[gene_id_members]).intersection(set(funct_score[gene_id_data]))
        self.functional_score.df = funct_score[funct_score[gene_id_data].isin(gene_i)]
        self.functional_score.df = self.functional_score.df.rename(columns={gene_id_data: gene_id_members})
        self.functional_score.meta = {"merge_gene_id_members": "no-merge",
                                      "gene_id": gene_id_members}

        print(
            "***Of {} {} members {} (per {}) have at least one phosphosite with functional score in the functional "
            "score dataset.".format(len(self.members[gene_id_members].unique()),
                                    self.name, len(gene_i), gene_id_members))

        # make a dictionary with the number of phosphosites with functional score for each gene
        func_score_counts_dict = self.functional_score.df[gene_id_members].value_counts().to_dict()

        func_score_counts = self.convert_id_count_table(func_score_counts_dict, gene_id_members,
                                                        "functionalscore_count")
        self.add_column_counts(func_score_counts, "functionalscore_count")

        # make a position column in the phosphosite data frame
        regul_gene_id = self.regulation_annot.meta["gene_id"]
        self.regulation_annot.df["position"] = self.regulation_annot.df["MOD_RSD"].apply(
            lambda x: int(re.match(r'\D*(\d*)-.*', x).group(1)))
        self.regulation_annot.df[regul_gene_id + "-position"] = self.regulation_annot.df[regul_gene_id] + \
                                                                self.regulation_annot.df["position"].astype(str)

        self.functional_score.df[gene_id_members + "-position"] = self.functional_score.df[gene_id_members] + \
                                                                  self.functional_score.df["position"].astype(str)

        # create a big dataframe containing the all phosphosites from PSP, their regulatory annotation and all
        # phosphosites with functional score for genes in the set (outer merge)
        self.regulatory_union = self.members.merge(self.regulation_annot.df, on=regul_gene_id)
        self.regulatory_union = self.regulatory_union.merge(
            self.functional_score.df[[gene_id_members + "-position", "functional_score"]],
            left_on=regul_gene_id + "-position", right_on=gene_id_members + "-position", how="outer")

        # get number of phosphosites in PSP with functional score for each gene
        tmp = self.regulatory_union.loc[(~self.regulatory_union["functional_score"].isna()) &
                                        (~self.regulatory_union["MOD_RSD"].isna()), :]

        tmp_counts = tmp[self.gene_id_base].value_counts().to_dict()
        tmp_counts = {i: tmp_counts[i] if i in tmp[self.gene_id_base].values else np.nan
                      for i in self.members[self.gene_id_base]}
        tmp = pd.DataFrame.from_dict(tmp_counts, orient='index').reset_index()
        tmp.columns = [self.gene_id_base, "functionalscore_count_overlapPSP"]

        self.add_column_counts(tmp, "functionalscore_count_overlapPSP")

        # get number of phosphosites with regulatory annotation in PSP and with functional score for each gene
        tmp = self.regulatory_union.loc[(~self.regulatory_union["functional_score"].isna()) &
                                        ((~self.regulatory_union["ON_PROCESS"].isna()) |
                                         (~self.regulatory_union["ON_FUNCTION"].isna())
                                         ), :]

        # make a dictionary with the number of phosphosites with a high functional score and a regulatory effect
        tmp_count = tmp[self.gene_id_base].value_counts().to_dict()
        tmp_count = {i: tmp_count[i] if i in tmp[self.gene_id_base].values else np.nan for i in
                     self.members[self.gene_id_base]}
        tmp = pd.DataFrame.from_dict(tmp_count, orient='index').reset_index()
        tmp.columns = [self.gene_id_base, f"functionalscore+regulatory_count"]

        self.add_column_counts(tmp, f"functionalscore+regulatory_count")

    def add_protein_expression(self, express, gene_id_data="pax_db_string", gene_id_members="pax_db_string",
                               express_cols=[]):
        """
        Subsets the protein abundance dataset to genes in the protein family. Retrieves the protein abundance for
        each gene.
        The subsetted dataframe will be stored as `self.protein_expression.df`. `gene_id_members` will be used to
        denote the type of gene identifier.
        
        Parameters
        ----------
        express : pandas.DataFrame
            Dataframe containing per row the protein expression value of one protein.
        gene_id_data : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `phospho` dataframe.
        gene_id_members : str
            Type of gene identifier that should be used for subsetting. The identifier should be present as a column
            name in the `self.members` dataframe.
        express_cols : list
            Which columns of the gene abundance dataset should be used. If empty list is passed all columns are used
            for the merge.
        """
        # make sure both the expression dataset and members dataframe have the given gene_id as a column
        if gene_id_members not in self.members.columns or gene_id_data not in express.columns:
            raise KeyError(
                f"Make sure that {gene_id_data} is a column name of the `express` dataframe"
                f"and {gene_id_members} is a column name in the `self.members` dataframe.")

        print("The set '{}' contains {} members (gene_id={}).".format(self.name,
                                                                      len(self.members[gene_id_members].unique()),
                                                                      gene_id_members))
        print("The protein abundance dataset contains {} unique entries of expression information for {} unique "
              "proteins.".format(len(express[gene_id_data]), len(set(express[gene_id_data]))))

        if len(express_cols) < 1:
            express_cols = express.columns

        # subset the expression information to only the members in the protein set
        gene_i = set(self.members[gene_id_members]).intersection(set(express[gene_id_data]))
        self.protein_expression.df = self.members.merge(express[express_cols], left_on=gene_id_members,
                                                        right_on=gene_id_data)
        self.protein_expression.meta = {"merge_gene_id_members": gene_id_members}

        print("***Of {} {} members {} have expression information in the protein abundance dataset.".format(
            len(self.members[gene_id_members].unique()), self.name, len(gene_i), self.protein_expression.df.shape[0]))

    def get_highlyfunctionalsites(self, funct_thrs=0.5):
        """
        Will get the number of phosphosites with a functional score > the defined threshold per gene. In addition
        the number of phosphosites with both high functional score and regulatory information is reported.
        
        Parameters
        ----------
        funct_thrs : float
            Every phosphosite with a functional score > funct_thrs will be selected.
        """

        # add a column indicating how many phosphosites on a protein are "highly functional"
        self.regulatory_union[f"score>{funct_thrs}"] = self.regulatory_union["functional_score"] > funct_thrs

        # make a dictionary with the number of highly functional phosphosites with functional score for each TF
        tmp = self.regulatory_union[self.regulatory_union[f"score>{funct_thrs}"] == True]
        tmp_counts = tmp[self.gene_id_base].value_counts().to_dict()

        funct_score_id = self.functional_score.meta["gene_id"]
        funct_score_genes = self.members.loc[self.members[funct_score_id].isin(self.functional_score.df[
            funct_score_id].unique()), self.gene_id_base].unique()

        tmp_counts = {i: tmp_counts[i] if i in tmp[self.gene_id_base].values else 0 if i in funct_score_genes else np.nan
                      for i in self.members[self.gene_id_base].unique()}
        tmp = pd.DataFrame.from_dict(tmp_counts, orient='index').reset_index()
        tmp.columns = [self.gene_id_base, f"functionalscore>{funct_thrs}_count"]

        self.add_column_counts(tmp, f"functionalscore>{funct_thrs}_count")

        # get phosphosites that are both annotated with a high functional score and a regulatory effect
        tmp = self.regulatory_union.loc[(self.regulatory_union["functional_score"] > funct_thrs) &
                                        ((~self.regulatory_union["ON_PROCESS"].isna()) |
                                         (~self.regulatory_union["ON_FUNCTION"].isna())
                                         ), :]

        # make a dictionary with the number of phosphosites with a high functional score and a regulatory effect
        tmp_count = tmp[self.gene_id_base].value_counts().to_dict()
        tmp_count = {i: tmp_count[i] if i in tmp[self.gene_id_base].values else np.nan for i in
                     self.members[self.gene_id_base].unique()}
        tmp = pd.DataFrame.from_dict(tmp_count, orient='index').reset_index()
        tmp.columns = [self.gene_id_base, f"functionalscore>{funct_thrs}+regulatory_count"]

        self.add_column_counts(tmp, f"functionalscore>{funct_thrs}+regulatory_count")

    def return_high_metric_genes(self, metric, thrs, gene_id="default"):
        """
        Will return a list of genes with the specified gene_id that have a value > the threshold in the given
        metric. A metric has to be a column name in `self.counts.columns`. This column needs to numeric.
        
        Parameters
        ----------
        metric : str
            Name of a numeric column in `self.counts.columns`.
        thrs : float
            Proteins that have a value > than the thrs in the given metric are selected.
        gene_id : str
            Type of gene identifier that should be used to return the selected genes
        """
        if gene_id == "default":
            gene_id = self.gene_id_base

        return self.counts.loc[self.counts[metric] > thrs, gene_id].tolist()

    def get_unique_dataset(self, dataframe_obj, intr_columns=[]):
        """
        Returns the requested dataset but removes duplicated rows respective to the ID that was used for
        originally merging the dataset with the set members.

        Parameters
        ----------
        dataframe_obj : DataframeMetaObject
            DataframeMeta object that contains has a dataframe (`df`) and metadata (`meta`) variable.
        intr_columns : list, optional
            List of columns in the dataframe that should be retrieved, if empty all columns are returned.
        """
        ID = dataframe_obj.get_metadata("merge_gene_id_members")

        # remove duplicated rows in terms of the merging ID and the columns interested in
        tmp = dataframe_obj.df.copy()
        if len(intr_columns) == 0:
            tmp = tmp[~tmp.duplicated(subset=[ID])]
        else:
            tmp = tmp.loc[~tmp.duplicated(subset=[ID]), intr_columns]

        return tmp
