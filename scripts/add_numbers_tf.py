import argparse
import pandas as pd
import numpy as np
import pickle
import re
from datetime import date
import os

from module.ProteinSet import ProteinSet

parser = argparse.ArgumentParser(description="Included TF resources")
parser.add_argument("--resource",
                    choices=["Lambert", "Dorothea", "Trrust", "all"],
                    type=str,
                    help="Resources used for curating a list of TFs")
parser.add_argument("--date",
                    type=str,
                    help="Date when pubmed count for resource was retrieved")

args = parser.parse_args()
if args.resource is None:
    resource = "all"
else:
    resource = args.resource
if args.date is None:
    DATE = date.today().strftime("%Y%m%d")
else:
    DATE = args.date

OUTPUT = "../output/"
if resource != "all":
    OUTPUT += resource + "/"

INPUT = "../input/"

#################################
# LOAD DATAFRAMES
#################################

# load the InterPro families with the protein counts
InterPro_families = pd.read_csv("../output/0_InterPro_human_families_20210309.csv")
interpro_mapping = {acc: InterPro_families.at[i, "name"]
                    for i, acc in enumerate(InterPro_families["accession"])}

# load the human proteome
human_proteome = pd.read_csv("../output/0_Uniprot_humanproteome_Interpro_20210309.tsv",
                             delimiter="\t",
                             usecols=["Entry", "Protein names", "Gene names", "Cross-reference (InterPro)"])
human_proteome.columns = ['uniprot_ACC', 'protein_names', 'uniprot_entryname', 'InterPro']
# only consider proteins that have interpro annotation
human_proteome_interpro = human_proteome.loc[~human_proteome["InterPro"].isna()]

# load curated TFs list
TFs_curated = pd.read_csv(OUTPUT + "2_TF_interpro_family_annotation.csv")
TFs_curated["InterPro_fam_list"] = [i if i is np.nan else [j for j in i.split(";")[:-1]]
                                    for i in TFs_curated["InterPro_fam"]]

###
# TF target genes
###
# get number of target genes stored in Dorothea
dorothea = pd.read_csv(INPUT + "Dorothea_human_TFregulon.csv")
dorothea.columns = ["HGNC_symbol", "confidence", "target", "mor"]

# get number of target genes stored in TRRUST
trrust = pd.read_csv(INPUT + "trrust_rawdata.human.tsv", sep="\t", header=None)
trrust.columns = ["HGNC_symbol", "target", "activator-repressor", "PMID"]


###
# TF interaction partners
###
def find_uniprot_id(row, a):
    if a:
        splt = re.split(':|\|', row["#ID(s) interactor A"])
        if "uniprotkb" in splt:
            ret = splt[splt.index("uniprotkb") + 1]
        else:
            splt = re.split(':|\|', row["Alt. ID(s) interactor A"])
            if "uniprotkb" in splt:
                ret = splt[splt.index("uniprotkb") + 1]
            else:
                ret = np.nan
    else:
        splt = re.split(':|\|', row["ID(s) interactor B"])
        if "uniprotkb" in splt:
            ret = splt[splt.index("uniprotkb") + 1]
        else:
            splt = re.split(':|\|', row["Alt. ID(s) interactor B"])
            if "uniprotkb" in splt:
                ret = splt[splt.index("uniprotkb") + 1]
            else:
                ret = np.nan
    return ret


intact = pd.read_csv(INPUT + "IntAct/intact.txt", sep="\t")

# add columns for uniprot ID for each Interactor
intact["InteractorA_uniprot"] = intact.apply(lambda x: find_uniprot_id(x, a=True), axis=1)
intact["InteractorB_uniprot"] = intact.apply(lambda x: find_uniprot_id(x, a=False), axis=1)

# remove rows where one of the interactors has no uniprot ID -> can't be mapped to the TF list
intact = intact.loc[~intact["InteractorA_uniprot"].isna() |
                    ~intact["InteractorB_uniprot"].isna()]

###
# TF phosphosites
###
phosphositeplus = pd.read_table(INPUT + "PhosphositePlus/Phosphorylation_site_dataset.tsv", sep="\t")
# only keep the human records
phosphositeplus = phosphositeplus.loc[phosphositeplus["ORGANISM"] == "human"]

phosphositeplus_regulatory = pd.read_table(INPUT + "PhosphositePlus/Regulatory_sites.tsv", sep="\t")
# only keep the human records
phosphositeplus_regulatory = phosphositeplus_regulatory.loc[phosphositeplus_regulatory["ORGANISM"] == "human"]

# only keep entries regarding phosphorylation: MOD_RSD residue-p
phosphositeplus_regulatory = phosphositeplus_regulatory.loc[
    phosphositeplus_regulatory["MOD_RSD"].str.match('.*-p$') == True]

funct_score_Ochoa = pd.read_excel(INPUT + "Ochoa2020_functionalScoresPhosphosites.xlsx")

###
# TF literature annotations - pubmed hits
###
pubmed = pd.read_csv(OUTPUT + f"1_TF_pubmedcount_tit-abstr_{DATE}.csv")

###
# TF binding sites
###
# number of predicted bindings sites in HOCOMOCO
hocomoco = pd.read_csv(INPUT + "chromatin_binding/TFBShocomoco11Full.txt", sep="\t", header=None)
hocomoco.columns = ["uniprot_entryname", "motif_count"]
# the gene names contain a white space -> strip it
hocomoco["uniprot_entryname"] = hocomoco["uniprot_entryname"].str.strip()

# number of ChIP-seq peaks in ReMap
remap = pd.read_csv(INPUT + "chromatin_binding/ReMap2020peaksFiltered.tsv", sep="\t")
remap.columns = ["ChIPseq_peaks", "uniprot_entryname"]

#################################
# GET COUNT TABLE FOR TFS
#################################

TFs_curated_set = ProteinSet(TFs_curated, name="curated_TFs")

TFs_curated_set.add_phosphosites(phosphositeplus,
                                 gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
TFs_curated_set.add_regulationannot(phosphositeplus_regulatory,
                                    gene_id_data="ACC_ID", gene_id_members="uniprot_ACC")
TFs_curated_set.add_functionalscore(funct_score_Ochoa,
                                    gene_id_data="uniprot", gene_id_members="uniprot_ACC")
TFs_curated_set.get_highlyfunctionalsites(funct_thrs=0.5)
TFs_curated_set.add_target_genes(dorothea, count_col='Dorothea_targets',
                                 gene_id_data="HGNC_symbol", gene_id_members="HGNC_symbol")
TFs_curated_set.add_target_genes(trrust, count_col='TRRUST_targets',
                                 gene_id_data="HGNC_symbol", gene_id_members="HGNC_symbol")
TFs_curated_set.add_counts(pubmed, count_col='pubmed_count',
                           gene_id_data="HGNC_symbol", gene_id_members="HGNC_symbol")
TFs_curated_set.add_counts(remap, count_col='ChIPseq_peaks',
                           gene_id_data="uniprot_entryname", gene_id_members="uniprot_entryname")
TFs_curated_set.add_counts(hocomoco, count_col='motif_count',
                           gene_id_data="uniprot_entryname", gene_id_members="uniprot_entryname")
TFs_curated_set.add_interaction_partners(intact,
                                         gene_id_data="uniprot", gene_id_members="uniprot_ACC")

# remove potentially duplicated rows
TFs_curated_set.counts = TFs_curated_set.counts.drop_duplicates()

# add the source column
tmp = TFs_curated_set.counts.merge(TFs_curated[["HGNC_symbol", "source"]].drop_duplicates(),
                                   how="left", on="HGNC_symbol")

tmp.to_csv(OUTPUT + "3_TF_counts.csv", index=False)

# save the TF families dictionary as an object
if not os.path.exists(OUTPUT + "protein_sets"):
    os.makedirs(OUTPUT + "protein_sets")
with open(OUTPUT + "protein_sets/TFs_curated_set.pickle", 'wb') as f:
    pickle.dump(TFs_curated_set, f)
