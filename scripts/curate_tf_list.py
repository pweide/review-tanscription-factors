import argparse
import os
import pandas as pd
from module.gene_id_mapping import GeneIdMapping

parser = argparse.ArgumentParser(description="Included TF resources")
parser.add_argument("--resource",
                    choices=["Lambert", "Dorothea", "Trrust", "all"],
                    type=str,
                    help="Resources used for curating a list of TFs")

args = parser.parse_args()
if args.resource is None:
    resource = "all"
else:
    resource = args.resource

# load the human identifier mapping table
mapping_table = GeneIdMapping("../input/biomart_id_mapping.csv")

INPUT = "../input/"
OUTPUT = "../output/"
if resource != "all":
    if not os.path.exists(OUTPUT + resource):
        os.makedirs(OUTPUT + resource)
    OUTPUT += resource + "/"

if resource in ["Lambert", "all"]:
    # read list of TFs compiled by Lambert et al. 2020
    TFs_lambert = pd.read_csv(INPUT + "Lambert2018_HumanTFsFullDatabase.csv", index_col=0)
    TFs_lambert = TFs_lambert.loc[TFs_lambert['Is TF?'] == "Yes"]
    TFs_lambert["HGNC_symbol"] = [i.strip(" ") for i in TFs_lambert["HGNC symbol"]]
    TFs_lambert = TFs_lambert["HGNC_symbol"].tolist()
else:
    TFs_lambert = []

if resource in ["Dorothea", "all"]:
    # read the TF-regulon table defined by DoRothEA
    TFs_dorothea = pd.read_csv(INPUT + "Dorothea_human_TFregulon.csv")
    TFs_dorothea = TFs_dorothea["tf"].unique().tolist()
else:
    TFs_dorothea = []

if resource in ["Trrust", "all"]:
    # read the TF-target gene table by TRRUST
    TFs_trrust = pd.read_csv(INPUT + "trrust_rawdata.human.tsv", sep="\t", header=None)
    TFs_trrust.columns = ["tf", "target", "mor", "unknown_col"]
    TFs_trrust = TFs_trrust["tf"].unique().tolist()
else:
    TFs_trrust = []

TFs_union = set(TFs_lambert).union(TFs_dorothea).union(TFs_trrust)

# not clear if the TFs in Dorothea and TRRUST are given in HGNC symbol or uniprot gene name
# intersection with lambert was largest when considering them HGNC symbols
TFs_curated = mapping_table.convert_id(TFs_union, id_from="HGNC_symbol", id_to="all", retain_nonmap=True)

# save the source of the transcription factor
TFs_curated["source"] = ""
TFs_curated.loc[TFs_curated["HGNC_symbol"].isin(TFs_lambert), "source"] = "Lambert (2018)"
tmp = TFs_curated.loc[TFs_curated["HGNC_symbol"].isin(TFs_dorothea), "source"].copy()
TFs_curated.loc[TFs_curated["HGNC_symbol"].isin(TFs_dorothea), "source"] = tmp + ", Dorothea"
tmp = TFs_curated.loc[TFs_curated["HGNC_symbol"].isin(TFs_trrust), "source"].copy()
TFs_curated.loc[TFs_curated["HGNC_symbol"].isin(TFs_trrust), "source"] = tmp + ", TRRUST"
TFs_curated["source"] = TFs_curated["source"].str.strip(", ")

# save the genes with no mapping in the mapping table also in another table
non_mapping_tfs = TFs_curated[["ensembl_ID", "HGNC_ID", "uniprot_ACC", "uniprot_entryname"]].isna().all(axis=1)

TFs_curated.to_csv(OUTPUT + "0_curated_TFs.csv", index=False)
TFs_curated.loc[non_mapping_tfs, ["HGNC_symbol", "source"]].to_csv(OUTPUT + "0_curated_TFs_nomaps.csv", index=False)
