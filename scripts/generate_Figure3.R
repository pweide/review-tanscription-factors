library(tidyverse)
library(magrittr)


trr <- read_tsv("input/trrust_rawdata.human.tsv",
  col_names = c("TF", "Target", "Direction", "PMID"),
  na = "Unknown"
)


trr %<>% group_by(TF) %>%
  summarise(
    TRRUST_activating = sum(Direction == "Activation", na.rm = T),
    TRRUST_repressing = sum(Direction == "Repression", na.rm = T)
  )


ro <- cor(log(trr$TRRUST_activating), log(trr$TRRUST_repressing), method = "spearman")


get_density <- function(x, y, ...) {
  d <- cbind(x, y)
  apply(d, MARGIN = 1, FUN = function(i) sum((i[1] == d[, 1]) & (i[2] == d[, 2])))
}


trr$density <- get_density(trr$TRRUST_activating, trr$TRRUST_repressing)


trr %<>% mutate(
  TFforce = if_else((TRRUST_activating <= 1 | TRRUST_repressing <= 1) &
    !(TRRUST_activating == 0 & TRRUST_repressing == 0) &
    (density <= 3),
  TF,
  ""
  ),
  TFdyn = if_else(TFforce == "", TF, "")
)


pos <- position_jitter(
  width = ggplot2:::resolution(log10(trr$TRRUST_activating), FALSE) * 1,
  height = ggplot2:::resolution(log10(trr$TRRUST_repressing), FALSE) * 1,
  seed = 1
)


my_squish <- function(x, range = c(0, 1)) {
  x[x == -Inf] <- -0.30103
  x
}


cairo_pdf(file = "manuscript/figures/Figure3_raw.pdf", width = 12, height = 12)
ggplot(
  data = trr,
  aes(
    x = TRRUST_activating,
    color = log(density),
    y = TRRUST_repressing
  )
) +
  geom_point(position = pos) +
  scale_color_continuous(high = "#132B43", low = "#56B1F7") +
  scale_x_log10(
    oob = my_squish,
    breaks = c(.5, 1, 3, 10, 100),
    labels = c("0", "1", "3", "10", "100")
  ) +
  scale_y_log10(
    oob = my_squish,
    breaks = c(.5, 1, 3, 10, 50),
    labels = c("0", "1", "3", "10", "50")
  ) +
  coord_fixed() +
  annotate("label",
    x = 2.5,
    y = 50,
    label = paste0(
      "Spearman's ρ=",
      round(ro, 2)
    ),
    size = 10
  ) +
  ggrepel::geom_text_repel(
    aes(label = TFdyn),
    max.overlaps = 3,
    min.segment.length = 0,
    position = pos,
    col = "black",
    max.time = 5,
    max.iter = 10^6
  ) +
  ggrepel::geom_text_repel(
    aes(label = TFforce),
    max.overlaps = Inf,
    min.segment.length = 0,
    position = pos,
    col = "black",
    max.time = 5,
    max.iter = 10^6
  ) +
  ylab("# of repressing connections (log-scale)") +
  xlab("# of activating connections (log-scale)") +
  labs(color = "Log10 of number of TFs with\nthe same amount of connections") +
  theme_classic() +
  theme(
    legend.position = c(.85, .4),
    legend.background = element_rect(fill = "gray90", size = .5, linetype = "dotted"),
    axis.title = element_text(size = 20),
    axis.text.x = element_text(size = 18),
    axis.text.y = element_text(size = 18)
  )
dev.off()
