###
# Transcription factors: the bridge between cell signaling and gene regulation
# Figure 5
###

library(tidyverse)
library(openair)
library(here)
library(optparse)

option_list = list(
  make_option(c("-r", "--resource"), type="character", default="all", 
              help="resource used", metavar="character")
)

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

if (opt$resource == "all"){
  data <- "output/"
  output_figures <- "manuscript/0_figures/"
} else {
  data <- paste0("output/", opt$resource, "/")
  output_figures <- paste0("manuscript/", opt$resource, "/0_figures/")
}

# load the count table for various metrics 
tfs_counts <- read_csv(here(data, "3_TF_counts.csv")) %>% 
  rename(functionalscore = `functionalscore>0.5_count`)

plot_heatscatter <- function(xs, ys){
  plots <- list()
  eps <- 1e-07
  for (x in names(xs)){
    for (y in names(ys)){
      name <- paste0(x, "-", y)
      # only retain rows that are a unique combination of the two metrics and HGNC symbol
      tmp <- tfs_counts %>% 
        distinct(HGNC_symbol, !!sym(x), !!sym(y))
      # drop_na()
      # replace 0 counts with 0.7, since counts can only be integers 0.7 indicates a 0 count
      # none of the metrics used in Figure 5 have 0 counts -> can be omitted
      # tmp[[x]] <- recode(tmp[[x]], `0`=0.7)
      # tmp[[y]] <- recode(tmp[[y]], `0`=0.7)
      
      # collect number of TFs without NA
      x_TFs <- sum(!is.na(tmp[[x]]))
      y_TFs <- sum(!is.na(tmp[[y]]))
      
      x_lab <- sprintf("%s (n_TFs = %i)", xs[x], x_TFs)
      y_lab <- sprintf("%s (n_TFs = %i)", ys[y], y_TFs)
      
      # replace NA with "count of 0.01
      tmp[[x]][is.na(tmp[[x]])] <- 0.5
      tmp[[y]][is.na(tmp[[y]])] <- 0.5
      
      plots[name] <- scatterPlot(tmp, x = x, y = y, method = "hexbin", col= "viridis",
                                 ylim = c(0.3, 2*max(tfs_counts[[y]])), 
                                 log.y=TRUE, log.x=TRUE,
                                 xlab = x_lab, ylab = y_lab,
                                 ref.y = list(h=c(log10(0.51)), lty = 5, lwd = c(1.5), col=c("darkgrey")),
                                 ref.x = list(v=c(log10(0.51)), lty = 5, lwd = c(1.5), col=c("darkgrey")))
    }
  }
  return(plots)
}

# compare the coverage of literature curated resources
xs_lit = list("TRRUST_targets" = "TRRUST -\nTF targets",
              "ChIPseq_peaks" = "ReMap -\nChIPseq peaks")

ys_lit = list("regulation_count" = "PhosphoSitePlus -\nfunctional phosphosites")

plots_lit = plot_heatscatter(xs_lit, ys_lit)

# compare the coverage of unbiased resources
xs_unb = list("Dorothea_targets" = "DoRothEA -\nTF targets",
              "motif_count" = "HOCOMOCO -\nTF motifs")

ys_unb = list("phosphorylation_count" = "PhosphoSitePlus -\nphosphosites")

plots_unb = plot_heatscatter(xs_unb, ys_unb)

pdf(here(output_figures, "Figure5_raw.pdf"), height = 4 * 2, width = 5 * 2)
print(plots_lit[["TRRUST_targets-regulation_count"]], split = c(1, 1, 2, 2))
print(plots_lit[["ChIPseq_peaks-regulation_count"]], split = c(2, 1, 2, 2), newpage = FALSE)
print(plots_unb[["Dorothea_targets-phosphorylation_count"]], split = c(1, 2, 2, 2), newpage = FALSE)
print(plots_unb[["motif_count-phosphorylation_count"]], split = c(2, 2, 2, 2), newpage = FALSE)
dev.off()

