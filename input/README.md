# Input Resources

## Gene identifier mapping
Mapping table for all human genes (GRCh38.p12) was downloaded from <a href="https://www.ensembl.org/biomart/martview/" target="_blank">Ensembl's BioMart</a> using the Ensembl Genes 103 dataset. Two tables were downloaded:
1. mapping Gene stable ID (ensembl ID), HGNC ID, HGNC symbol
2. mapping Gene stable ID (ensembl ID), UniProtKB/Swiss-Prot ID, UniProtKB Gene Name symbol

The two tables were merged using an outer join based on ensembl ID.
* saved in `input/biomart_id_mapping.csv`

* please consult the Disclaimer provided by Ensembl when using Ensembl data: <a href="https://www.ensembl.org/info/about/legal/disclaimer.html" target="_blank">https://www.ensembl.org/info/about/legal/disclaimer.html</a>
* see following publications on Ensembl:
    * Andrew D Yates et al. Ensembl 2020. PubMed PMID: 31691826. doi:10.1093/nar/gkz966
    * Kinsella RJ, Kähäri A, Haider S, Zamora J, Proctor G, Spudich G, Almeida-King J, Staines D, Derwent P, Kerhornou A, Kersey P, Flicek P. Ensembl BioMarts: a hub for data retrieval across taxonomic space. Database (Oxford). Vol. 2011 Published online Jul 23, 2011


## Curated list of TFs
A list of putative human transcription factors (TFs) was compiled using following three resources.
If you want to recreate the tables, please download the original sources yourself and place them under the same name in the appropriate folder.
### Lambert et al. 2018
* Samuel A. Lambert et al., ‘The Human Transcription Factors’, Cell 172, no. 4 (08 2018): 650–65, https://doi.org/10.1016/j.cell.2018.01.029.
* downloaded on the 29th of May 2020: <a href="http://humantfs.ccbr.utoronto.ca/download.php" target="_blank">http://humantfs.ccbr.utoronto.ca/download.php</a> -> Human TFs -> Full
 Database
* save in `input/Lambert2018_HumanTFsFullDatabase.csv`

### DoRothEA
* downloaded on the 8. June 2020: <a href="https://github.com/saezlab/dorothea/blob/master/data/dorothea_hs.rda" target="_blank">https://github.com/saezlab/dorothea/blob/master/data/dorothea_hs.rda</a>, converted to
 csv using R by loading the rda file and using the `write.csv` function.
* columns (s. <a href="https://saezlab.github.io/dorothea/reference/dorothea_hs.html" target="_blank">https://saezlab.github.io/dorothea/reference/dorothea_hs.html</a>):
    * `tf`: TF identifier as HGNC symbols
    * `confidence`: Summary confidence score classifying regulons based on their quality (range is from A (high
     quality) to E (low quality))
    * `target`: target identifier as HGNC symbols
    * `mor`: mode of regulation indicating the effect of a TF on the target (signed upregulating or downregulating)
* save in `input/Dorothea_human_TFregulon.csv`

### TRRUST
* downloaded on the 9.June 2020: <a href="https://www.grnpedia.org/trrust/downloadnetwork.php" target="_blank">https://www.grnpedia.org/trrust/downloadnetwork.php</a> -> Human in tsv format
* save in `input/trrust_rawdata.human.tsv`

## Different metrics for TF annotation in databases
In our review we present the annotation of TFs with respect to several resources.
### IntAct
* downloaded on the 9. June 2020: <a href="https://www.ebi.ac.uk/intact/downloads" target="_blank">https://www.ebi.ac.uk/intact/downloads</a> -> `intact.zip`
* save extracted content in `input/IntAct/` without renaming any of the files

### PhosphoSitePlus
#### All phosphosites
* downloaded on 29. May 2020: <a href="https://www.phosphosite.org/staticDownloads" target="_blank">https://www.phosphosite.org/staticDownloads</a> -> `Phosphorylation_site_dataset.gz`
* save in `input/PhosphositePlus/Phosphorylation_site_dataset.gz`
* columns:
    * `LT_LIT` is the number of publications supporting the phosphorylation site.
    * `MS_LIT` is the number of mass spec studies supporting the site.
    * `MS_CST` is the number of mass spec studies performed by Cell Signaling Technology (Corporation that powers phosphositePlus)

#### Functional phosphosites
* downloaded on 29. May 2020: <a href="https://www.phosphosite.org/staticDownloads" target="_blank">https://www.phosphosite.org/staticDownloads</a> -> `Regulatory_sites.gz`
* save in `input/PhosphositePlus/Regulatory_sites.gz`

### Ochoa et al. 2020 - predicted functional phosphosites
* downloaded Supplementary Table 3 from <a href="https://doi.org/10.1038/s41587-019-0344-3" target="_blank">https://doi.org/10.1038/s41587-019-0344-3</a>
* rename the file and save in `input/Ochoa2020_functionalScoresPhosphosites.xlsx`

### Chromatin binding
#### ReMap - ChIP-seq peaks
* call script `scripts/ReMap_count_peaks_MacOS.sh` or `scripts/ReMap_count_peaks_Linux.sh` from a terminal, depending on your operating system. We ran the script on the 10. February 2021 using the Linux script. We couldn't test it on a Windows machine.
    * the script filters out names that include "phosph" to filter out phosphoproteomic studies
    * the script filters out all names with a "-" in the name, because these usually denote fusion proteins in cancer lines and usually don't have a reference to other gene identifiers (HGNC, ensemble etc)
    * via this filtering we could loose a few TFs that have a "-" in their approved HGNC symbol like `NKX2-1`
* the script saves a table with the number of known ChIP-seq peaks per TF in `input/chromatin_binding/ReMap2020peaksFiltered.tsv`

#### HOCOMOCO - binding motifs
To estimate the number of predicted binding sites we used standalone version of
<a href="https://sourceforge.net/projects/pwmscan/" target="_blank">PWMscan</a> with <a href="http://bowtie-bio.sourceforge.net/index.shtml" target="_blank">Bowtie</a> (version 1.2.3) as a search engine.
  * used `pwm_scan_ucsc` function to scan GRCh38 for each human TF motif in <a href="https://hocomoco11.autosome.ru/final_bundle/hocomoco11/full/HUMAN/mono/HOCOMOCOv11_full_pwm_HUMAN_mono.tar.gz" target="_blank">HOCOMOCOv11</a> with parameters (`-b 0.29,0.21,0.21,0.29` and `-e 0.00001`)
  * counted the number of lines in each corresponding BED file, which corresponds to the number of bindings sites for each TF
  * saved the resulting table in: `input/chromatin_binding/TFBShocomoco11Full.txt`

Additional details available upon request.
