# Transcription factors: the bridge between cell signaling and gene regulation
This repository contains all scripts and links to the raw dat used to generate figures and tables
mentioned in the review.

Please cite:
Paula Weidemüller et al., ‘Transcription Factors: Bridge between Cell Signaling and Gene Regulation’, PROTEOMICS 21, no. 23–24 (2021): 2000034, https://doi.org/10.1002/pmic.202000034.

## Requirements
For the workflow, Figure 2 and Supplementary Figure 1 and 2 you need python (we used version 3.8.1) with following packages installed. Check the `python_requirements.txt` file for all packages and the exact versions we used (you can use `conda` to create a new environment with these packages).
* `matplotlib`
* `numpy`
* `pandas`
* `requests`
* `supervenn`

For Figure 3, 4 and 5 you need R (we used version >4) with the following packages installed. Check the `renv.lock` file for all packages and the exact versions used (you can use the `renv` package to install all packages from this file).
* `tidyverse`
* `openair`
* `here`
* `optparse`
* `ggrepel`

## Raw datasets
Please visit the README in the `input` folder for instructions on how to download the raw datasets from the different resources (e.g. PhosphoSitePlus, TRRUST, etc.). Place the downloaded datasets into the `input` folder as instructed there.

ALTERNATIVELY, if you are only interested in reproducing the figures and want to play around with the tables, you can skip downloading the raw datasets and also skip following the workflow below. Just jump to the Figures and Tables part.

## Workflow
After you downloaded all input datasets, follow these steps. Run the scripts using python (`.py`).

The scripts include the option to specify which resource should be used to define the list of TFs. The options are `Lambert`, `Dorothea`, `Trrust` and `all` (a union of all three resources). To use this features, call the scripts from command line and add the parameter `--resource`. If the parameter is not specified the script will use `all` as default. Call the script from within the `scripts` folder.
```
python <script>.py --resource Lambert
```
If a specific resource is specified (not `all`) a subfolder is created in `output` under the name of the resource (e.g. `output/Lambert`) and results are saved within.

1. Run `curate_tf_list.py` in the `scripts` folder. This will generate the list of curated TFs from the different sources and the mapping to different gene identifiers. The resulting table is saved in `output/0_curated_TFs.csv`.
    * `0_curated_TFs_nomaps.csv` is a table that contains the HGNC symbols of TFs that are not found in the mapping table (`input/biomart_id_mapping.csv`). For these TFs no numbers could be collected for number of (functional, predicted functional) phosphosites, interacting proteins, ChIP-seq peaks and DNA binding sites.
2. Run `retrieve_interpro_family_annotations.py` in the `scripts` folder. This will download
    * a table of protein families found in the human genome annotated in InterPro: `output/0_InterPro_human_families_{DATE}.csv`, where DATE is the date on which the table was downloaded.
    * another table from uniprot that will contain for each gene the annotated InterPro protein families: `output/0_UniProt_humanproteome_Interpro_{DATE}.tsv`, where DATE is the date on which the table was downloaded
3. Run `get_tf_pubmedcount.py` in the `scripts` folder. This will search for the number of pubmed publications that mention a TF HGNC symbol in either title or abstract. The resulting table is saved in `output/1_TF_pubmedcount_tit-abstr_{DATE}.csv`, where DATE is the date on which the script was run.
4. Run `annotate_tf_interpro.py` in the `scripts` folder. This will annotate the interpro families for each TF. The resulting table is saved in `output/2_TF_interpro_family_annotation.csv`.
5. Run `add_numbers_tf.py` in the `scripts` folder. This will annotate each TF with the numbers in the different metrics (e.g. number of phosphosites, target genes). The count table is saved in `output/3_TF_counts.csv`. Additionally a pickled ProteinSet object is saved in `output/protein_sets/TFs_curated_set.pickle`. This eases access to the metric counts for further analysis.
    * in addition to the `--resource` argument you can also set the `--date` argument, this is the date of the `1_TF_pubmedcount_tit-abstr_{DATE}.csv` file (s. point 3.).
6. Run `add_numbers_tf_protfamilies.py` in the `scripts` folder. This will annotate the ten most common TF protein families and ten most common human protein families with the number of phosphosites (measured, functional, predicted functional).
    * For TF protein families, a pickled ProteinSet object is saved in `output/protein_sets/TFs_families_merged_sets_TFs.pickle`. Additionally, images are saved that show the overlap of TFs in the different protein families (`TFs_families_10_before_merge.png`). Users can choose to merge highly similar protein sets (sets that share a lot of proteins). We didn't merge any sets. Thus `TFs_families_10_mergedfam.txt` is empty and `TFs_families_10_after_merge.png` is identical to `TFs_families_10_before_merge.png`.
    * For human protein families, a pickled ProteinSet object is saved in `output/protein_sets/protein_families_merged_sets.pickle.pickle`. Additionally, images are saved that show the overlap of TFs in the different protein families (`protein_families_10_before_merge.png`). Users can choose to merge highly similar protein sets (sets that share a lot of proteins). We didn't merge any sets. Thus `protein_families_10_mergedfam.txt` is empty and `protein_families_10_after_merge.png` is identical to `protein_families_10_before_merge.png`.

After following all these steps all data needed for plotting is available.

## Figures
### Figure 2
Use `Review_numbers_visualisations.ipynb` or see `Review_numbers_visualisations.pdf` in the `scripts` folder to generate the Figure. The raw figure was saved in `manuscript/0_figures/`. The figure was slightly altered (e.g. adding A,B,C,...) with Adobe Illustrator. The final version used in the review is found in `manuscript/`.

### Figure 3
Run `scripts/generate_Figure3.R` using R. The raw figure is saved in `manuscript/0_figures/`.

### Figure 4,5
Run `scripts/generate_Figure4.R` and `scripts/generate_Figure5.R` using R. The raw figures are saved in `manuscript/0_figures/`. There is a command line option to specify the resource for which to draw the plots, options are `Lambert`, `Dorothea`, `Trrust` and `all` (a union of all three resources - default). Run like:
```
Rscript scripts/generate_Figure4.R --resource Lambert
```
For `Lambert`, `Dorothea` and `Trrust` a subfolder in `manuscript/` with the same name is needed. Plots are saved within.

The figures were slightly altered (e.g. adding A,B,C,...) with Adobe Illustrator. The final version used in the review are found in `manuscript/`.

## Tables
Use `Review_numbers_visualisations.ipynb` or see `Review_numbers_visualisations.pdf` in the `scripts` to generate the tables. Tables are saved in `manuscript/0_tables/`. Please see the associated `manuscript/0_tables/README.txt` file for explanation of the columns.
