# Supplementary Table 1:
This table contains our curated list of human transcription factors and also contains their annotated InterPro protein families. Gene identifiers were mapped using ensembl’s BioMart service (Kinsella et al, 2011). It has the following columns:
- HGNC_symbol: TF name as defined by HUGO Gene Nomenclature Committee (HGNC) as given by the resource
- HGNC_symbol_new: some TF HGNC symbols as given by resource were outdated, here we manually added the latest HGNC symbol
- ensembl_ID: ensembl identifier corresponding to the HGNC symbol of a TF
- HGNC_ID: HGNC identifier corresponding to the HGNC symbol of a TF
- uniprot_ACC: uniprot accession number corresponding to the HGNC symbol of a TF using only reviewed entries (SwissProt)
- uniprot_entryname: uniprot entry name mapping to the HGNC symbol of a TF using only reviewed entries (SwissProt)
- source: source that defined a TF as a TF
- InterPro_fam_name: a list of semicolon (;) separated protein family names as assigned by InterPro
- InterPro_fam: a list of semicolon (;) separated protein family identifiers as assigned by InterPro, the order corresponds to the order in InterPro_fam_name
- manually_edited: 'yes' indicates that the entry was edited to manually add latest HGNC symbols or correct ensembl IDs

# Supplementary Table 2:
This table contains the number of functional phosphosites for each TF in our curated set for which data was available along with the functional annotation of the phosphosites (if available). It has the following columns:
- uniprot_ACC: uniprot accession number corresponding to the HGNC symbol of a TF using only reviewed entries (SwissProt)
- ensembl_ID: ensembl ID corresponding to the uniprot accession number, for some uniprot ACC's multiple ensembl ID's are mapped, which are all shown and separated by a comma, e.g. 'ENSG00000284774, ENSG00000185122'
- HGNC_symbol: TF name as defined by HUGO Gene Nomenclature Committee (HGNC)
- regulatory_phosphosites: number of phosphosites with either an annotated effect on molecular function and/or biological process in PhosphoSitePlus
- ON_FUNCTION: a list of semicolon (;) separated effects of annotated phosphosites on molecular functions as stored in PhosphoSitePlus, this list displays the effects of all annotated phosphosites
- ON_PROCESS: a list of semicolon (;) separated effects of annotated phosphosites on biological processes as stored in PhosphoSitePlus, this list displays the effects of all annotated phosphosites

# Supplementary Table 3:
This table contains the functional data we collected for each of each TF in our curated list from different data sources. It contains the following columns:
- HGNC_symbol: TF name as defined by HUGO Gene Nomenclature Committee (HGNC)
- phosphorylation_count: number of measured phosphosites in PhosphoSitePlus (PSP)
- regulation_count: number of phosphosites with either an annotated effect on molecular function and/or biological process in PSP
- ON_FUNCTIONterms_count: number of molecular functions annotated to all TF phosphosites in PSP
- ON_FUNCTION_count: number of phosphosites with an annotated effect on molecular functions in PSP
- ON_PROCESSterm_count: number of biological processes annotated to all TF phosphosites in PSP
- ON_PROCESS_count: number of phosphosites with an annotated effect on biological processes in PSP
- functionalscore_count: number of phosphosites with a functional score calculated by (Ochoa et al, 2019)
- functionalscore_count_overlapPSP: number of phosphosites with a functional score calculated by (Ochoa et al, 2019) that are also found in PSP
- functionalscore+regulatory_count: number of phosphosites with a functional score calculated by (Ochoa et al, 2019) that are also annotated with a molecular function and/or biological process in PSP
- functionalscore>0.5_count: number of phosphosites with a functional score larger than 0.5 calculated by (Ochoa et al, 2019)
- functionalscore>0.5+regulatory_count: number of phosphosites with a functional score larger than 0.5 calculated by (Ochoa et al, 2019) that are also annotated with a molecular function and/or biological process in PSP
- Dorothea_targets: number of target genes stored in DoRothEA
- TRRUST_targets: number of target genes stored in TRRUST
- Pubmed_count: number of publications mentioning the HGNC symbol of a TF in either manuscript or title
- ChIPseq_peaks: number of ChIPseq peaks stored in ReMap
- motif_count: number of DNA binding motifs stored in HOCOMOCO
- intact_interactions: number of protein interactions stored in IntAct
